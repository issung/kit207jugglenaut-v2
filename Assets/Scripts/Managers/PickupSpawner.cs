﻿using System.Collections;
using UnityEngine;

public class PickupSpawner : UnitySingleton<PickupSpawner>
{
    public override bool Persistent => false;

    #pragma warning disable IDE0044, CS0649

    [SerializeField] private bool m_debug;

    public Bounds spawnBounds;
    public float minSpawnTime = 10f;
    public float maxSpawnTime = 15;
    public float startDelay = 15f;
    public int maxPickups = 4;

    [ShowOnly]
    public int currentPickups;

    private WaitForSeconds m_maxSpawnWait;
    private Coroutine m_spawnCoroutine;
    private BasicRandomList<GameObject> m_pickups;

    #pragma warning restore IDE0044, CS0649

    public override void Awake()
    {
        base.Awake();

        m_pickups = new BasicRandomList<GameObject>
        {
            PrefabLinks.Instance.pickupHealth,
            PrefabLinks.Instance.pickupFireRate,
            //PrefabLinks.Instance.pickupFirePower
            PrefabLinks.Instance.pickupShield
        };

        m_maxSpawnWait = new WaitForSeconds(maxSpawnTime);
    }

    public void StartSpawning()
    {
        m_spawnCoroutine = StartCoroutine(SpawnCoroutine());
    }

    private IEnumerator SpawnCoroutine()
    {
        yield return new WaitForSeconds(startDelay);

        while (true)
        {
            if (currentPickups < maxPickups)
            {
                Instantiate(
                    Utils.Chance(100) < 20 ?
                        PrefabLinks.Instance.pickupShield :
                    Utils.Chance(100) < 50 ?
                        PrefabLinks.Instance.pickupHealth :
                        PrefabLinks.Instance.pickupFireRate,
                    //m_pickups.Random,
                    new Vector3(
                        spawnBounds.center.x + Random.Range(-spawnBounds.extents.x, spawnBounds.extents.x),
                        spawnBounds.center.y + Random.Range(-spawnBounds.extents.y, spawnBounds.extents.y)),
                    Quaternion.identity);

                currentPickups++;
            }
            else
                yield return m_maxSpawnWait;

            yield return new WaitForSeconds(Random.Range(minSpawnTime, maxSpawnTime));
        }
    }

    private void OnDrawGizmos()
    {
        if (m_debug)
        {
            Gizmos.color = new Color(0f, 1f, 1f, 0.4f);
            Gizmos.DrawCube(spawnBounds.center, spawnBounds.size);
        }
    }
}