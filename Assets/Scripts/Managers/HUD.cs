﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HUD : UnitySingleton<HUD>
{
    public override bool Persistent => false;

    #pragma warning disable IDE0044, CS0649

    [Header("Game Information")]
    [SerializeField] private GameObject m_infoBackground;
    [SerializeField] private TextMeshProUGUI m_waveText;
    [SerializeField] private TextMeshProUGUI m_timeText;
    [SerializeField] private TextMeshProUGUI m_scoreText;

    [Header("Wave Bar")]
    [SerializeField] private Image m_waveBar;
    [SerializeField] private HorizontalLayoutGroup m_textParentLayout;
    [SerializeField] private TextMeshProUGUI m_enemiesLeftText;

    [Header("Wave Introduction")]
    [SerializeField] private GameObject m_waveIntroBackground;
    [SerializeField] private TextMeshProUGUI m_waveIntroText;

    [Header("Game Introduction")]
    [SerializeField] private GameObject m_gameIntro;
    [SerializeField] private TextMeshProUGUI m_gameIntroText;

    [Header("Game End")]
    [SerializeField] private GameObject m_gameEndScreen;
    [SerializeField] private TextMeshProUGUI m_gameEndText;

    #pragma warning restore IDE0044, CS0649

    private WaitForSeconds m_updateGameTimeTextInterval;
    private Coroutine m_updateGameTimeText;

    private float m_widthReference;
    private float m_timeStart;
    private float m_time;
    private int m_minutes, m_seconds, m_microseconds;
    
    public bool HasGameEnded { get; private set; }

    public override void Awake()
    {
        base.Awake();

        m_widthReference = GetComponentInParent<Canvas>().GetComponent<CanvasScaler>().referenceResolution.x;

        m_gameEndScreen.SetActive(false);
        UpdateWaveIntroText("");
        UpdateWaveBar();
        m_infoBackground.SetActive(false);
        m_gameIntroText.text = "Drag your ships around using the left mouse button.";
    }

    private void Start()
    {
        m_updateGameTimeTextInterval = new WaitForSeconds(1f);
    }

    public void StartGame()
    {
        m_gameIntro.SetActive(false);
        m_updateGameTimeText = StartCoroutine(UpdateGameTimeText());

        UpdateWaveText(WaveSpawner.Instance.visualCurrentWave);
    }

    private IEnumerator UpdateGameTimeText()
    {
        m_timeStart = Time.timeSinceLevelLoad;
        m_infoBackground.SetActive(true);

        while (true)
        {
            m_time = Time.timeSinceLevelLoad - m_timeStart;
            m_minutes = (int)(m_time / 60f);
            m_seconds = (int)m_time % 60;
            m_microseconds = (int)(m_time * 10f % 10);
            UpdateTime();

            yield return m_updateGameTimeTextInterval;
        }
    }

    public void UpdateWaveText(int wave) => m_waveText.text = $"Wave: {wave + 1}";

    public void UpdateWaveIntroText(string intro)
    {
        if (string.IsNullOrWhiteSpace(intro))
        {
            m_waveIntroText.text = "";
            m_waveIntroBackground.SetActive(false);
        }
        else
        {
            m_waveIntroText.text = intro;
            m_waveIntroBackground.SetActive(true);
        }
    }

    public void UpdateScore(int score) => m_scoreText.text = $"Score: {score:N0}";

    public string GetTime() => $"{m_minutes:D2}:{m_seconds:D2}";

    public void UpdateTime() => m_timeText.text = $"Time: {GetTime()}";

    public void UpdateWaveBar()
    {
        if (WaveSpawner.Instance.enemiesLeft > 0)
        {
            m_waveBar.fillAmount = WaveSpawner.Instance.enemiesLeft / (float)WaveSpawner.Instance.EnemiesInWave;
            m_enemiesLeftText.text = $"Enemies Left: {WaveSpawner.Instance.enemiesLeft:N0}";
            m_enemiesLeftText.rectTransform.sizeDelta = new Vector2(
                Mathf.Max(
                    m_enemiesLeftText.preferredWidth,
                    (m_widthReference * m_waveBar.fillAmount) - m_textParentLayout.padding.horizontal),
                m_enemiesLeftText.rectTransform.sizeDelta.y);

            m_waveBar.gameObject.SetActive(true);
        }
        else
        {
            m_waveBar.gameObject.SetActive(false);
            m_waveBar.rectTransform.localScale = Vector3.one;
            m_enemiesLeftText.text = "";
        }
    }
    
    public void SetGameEnded(bool won)
    {
        if (m_updateGameTimeText != null)
            StopCoroutine(m_updateGameTimeText);

        HasGameEnded = true;
        m_infoBackground.SetActive(false);
        m_gameEndScreen.SetActive(true);
        m_gameEndText.text =
            $"You {(won ? "won" : "lost")}!\nWave: {WaveSpawner.Instance.visualCurrentWave}\nTime: {GetTime()}\nScore: {Player.Score:N0}";

        Player.ResetStaticProperties();
    }
}