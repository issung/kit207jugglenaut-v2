﻿using UnityEngine;
using static UnityEngine.ParticleSystem;

public class ParticleManager : UnitySingleton<ParticleManager>
{
    [ShowOnly, SerializeField] private ParticleSystem m_sparks;
    [ShowOnly, SerializeField] private ParticleSystem m_smoke;
    //[ShowOnly, SerializeField] private ParticleSystem m_smokingSparks;
    [ShowOnly, SerializeField] private ParticleSystem m_plasmaExplosion;

    [ShowOnly, SerializeField] private GameObject m_sparksInst;
    [ShowOnly, SerializeField] private GameObject m_smokeInst;
    //[ShowOnly, SerializeField] private GameObject m_smokingSparksInst;
    [ShowOnly, SerializeField] private GameObject m_plasmaExplosionInst;

    [ShowOnly, SerializeField] private MainModule m_mainModule;

    public override void Awake()
    {
        base.Awake();

        m_sparksInst = Instantiate(PrefabLinks.Instance.sparkParticles);
        m_smokeInst = Instantiate(PrefabLinks.Instance.smokeParticles);
        //m_smokingSparksInst = Instantiate(PrefabLinks.Instance.smokingSparksParticles);
        m_plasmaExplosionInst = Instantiate(PrefabLinks.Instance.plasmaExplosionParticles);

        m_sparks = m_sparksInst.GetComponent<ParticleSystem>();
        m_smoke = m_smokeInst.GetComponent<ParticleSystem>();
        //m_smokingSparks = m_smokingSparksInst.GetComponent<ParticleSystem>();
        m_plasmaExplosion = m_plasmaExplosionInst.GetComponent<ParticleSystem>();

        DontDestroyOnLoad(m_sparksInst);
        DontDestroyOnLoad(m_smokeInst);
        //DontDestroyOnLoad(m_smokingSparksInst);
        DontDestroyOnLoad(m_plasmaExplosionInst);
    }

    public void Spawn(ParticleSystem system, Vector3 position)
    {
        m_mainModule = system.main;
        system.transform.position = position;
        system.Play();
    }

    public void SpawnSparks(Vector3 position)          => Spawn(m_sparks, position);
    public void SpawnSmoke(Vector3 position)           => Spawn(m_smoke, position);
    //public void SpawnSmokingSparks(Vector3 position)   => Spawn(m_smokingSparks, position);
    public void SpawnPlasmaExplosion(Vector3 position) => Spawn(m_plasmaExplosion, position);

    public GameObject CreateSmokingSparks(out ParticleSystem particleSystem)
    {
        GameObject obj = Instantiate(PrefabLinks.Instance.smokingSparksParticles);
        particleSystem = obj.GetComponent<ParticleSystem>();
        return obj;
    }
}