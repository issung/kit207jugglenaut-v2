﻿using TMPro;
using UnityEngine;

public class PrefabLinks : UnitySingleton<PrefabLinks>
{
    #region Entities

    public GameObject bulletPrefab;
    public GameObject playerShip;

    public GameObject enemyLight;
    public GameObject enemyGeneric;
    public GameObject enemyDodger;
    public GameObject enemyFast;
    public GameObject enemyTough;

    #endregion

    #region Pickups

    public GameObject pickupHealth;
    public GameObject pickupFireRate;
    //public GameObject pickupFirePower;
    public GameObject pickupShield;

    #endregion

    #region UI

    public GameObject healthBar;

    #endregion

    #region Particles

    public GameObject sparkParticles;
    public GameObject smokeParticles;
    public GameObject smokingSparksParticles;
    public GameObject plasmaExplosionParticles;

    #endregion

    #region Fonts

    public TMP_FontAsset BitDarlingGreen;
    public TMP_FontAsset BitDarlingOrange;
    public TMP_FontAsset BitDarlingRed;

    #endregion

    public GameObject GetEnemy(EnemyType type) =>
        type == EnemyType.Passive ? enemyLight :
        type == EnemyType.Generic ? enemyGeneric :
        type == EnemyType.Fast ? enemyFast :
        type == EnemyType.Dodger ? enemyDodger :
        type == EnemyType.Tough ? enemyTough : throw new System.Exception();
}