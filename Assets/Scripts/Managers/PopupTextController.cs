﻿using UnityEngine;

public class PopupTextController : UnitySingleton<PopupTextController>
{
    public GameObject popupTextPrefab;

    public static void Popup(string text, PopupText.TextColor color, Vector3 position)
    {
        var go = Instantiate(Instance.popupTextPrefab);
        var component = go.GetComponent<PopupText>();
        component.Set(text, color, position);
    }
}