﻿using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(AudioPlayer))]
public class AudioManager : UnitySingleton<AudioManager>
{
    [ShowOnly] public AudioPlayer player;
    [ShowOnly] public AudioPlayer shootPlayer;
    [ShowOnly] public AudioPlayer enemyHitPlayer;
    [ShowOnly] public AudioPlayer explosionPlayer;

    [ShowOnly] public AudioClip playerHit1;
    [ShowOnly] public AudioClip playerHit2;
    [ShowOnly] public AudioClip playerHit3;
    [ShowOnly] public AudioClip playerHit4;
    [ShowOnly] public AudioClip playerHit5;
    [ShowOnly] public AudioClip playerHit6;
    [ShowOnly] public AudioClip playerHit7;
    [ShowOnly] public AudioClip playerHit8;
    [ShowOnly] public AudioClip playerHit9;

    [ShowOnly] public AudioClip playerForceFieldHit;

    [ShowOnly] public AudioClip enemyHit1;
    [ShowOnly] public AudioClip enemyHit2;
    [ShowOnly] public AudioClip enemyHit3;

    [ShowOnly] public AudioClip explosion1;
    [ShowOnly] public AudioClip explosion2;
    [ShowOnly] public AudioClip explosion3;

    [ShowOnly] public AudioClip pickupX2;
    [ShowOnly] public AudioClip pickupHealth;
    [ShowOnly] public AudioClip pickupShield;

    [ShowOnly] public AudioClip uiConfirm1;
    [ShowOnly] public AudioClip uiConfirm2;
    [ShowOnly] public AudioClip uiConfirm3;
    [ShowOnly] public AudioClip uiBack1;
    [ShowOnly] public AudioClip uiBack2;

    [ShowOnly] public AudioClip musicIngame1;
    [ShowOnly] public AudioClip musicIngame2;

    [ShowOnly] public AudioClip uiBackNew;
    [ShowOnly] public AudioClip uiConfirmNew;

    [ShowOnly] public Dictionary<string, AudioClip> audioClips;

    [ShowOnly] public BasicRandomList<AudioClip> playerHitClips;
    [ShowOnly] public BasicRandomList<AudioClip> enemyHitClips;
    [ShowOnly] public BasicRandomList<AudioClip> explosionClips;

    [ShowOnly] public BasicRandomList<AudioClip> musicIngameClips;

    [ShowOnly] public BasicRandomList<AudioClip> uiConfirmClips;
    [ShowOnly] public BasicRandomList<AudioClip> uiBackClips;

    public override void Awake()
    {
        base.Awake();

        player = gameObject.AddComponent<AudioPlayer>();
        //player.audioSource.volume = 0.5f;
        player.audioSource.playOnAwake = false;

        shootPlayer = gameObject.AddComponent<AudioPlayer>();
        shootPlayer.SetVolume(0.1f);
        shootPlayer.MaxSoundsPerSecond = 0f;

        enemyHitPlayer = gameObject.AddComponent<AudioPlayer>();
        enemyHitPlayer.SetVolume(0.5f);
        enemyHitPlayer.MaxSoundsPerSecond = 1f / 16f;

        explosionPlayer = gameObject.AddComponent<AudioPlayer>();
        explosionPlayer.SetVolume(0.75f);
        explosionPlayer.MaxSoundsPerSecond = 1f / 16f;

        audioClips = new Dictionary<string, AudioClip>();
        var clips = Resources.LoadAll<AudioClip>("Audio");

        for (int i = 0; i < clips.Length; i++)
            audioClips.Add(clips[i].name, clips[i]);

        //ui_confirm = audioClips["ui_confirm"];
        //ui_cancel = audioClips["ui_cancel"];

        playerHitClips = new BasicRandomList<AudioClip>
        {
            (playerHit1 = audioClips["hit_1"]),
            (playerHit2 = audioClips["hit_2"]),
            (playerHit3 = audioClips["hit_3"]),
            (playerHit4 = audioClips["hit_4"]),
            (playerHit5 = audioClips["hit_5"]),
            (playerHit6 = audioClips["hit_6"]),
            (playerHit7 = audioClips["hit_7"]),
            (playerHit8 = audioClips["hit_8"]),
            (playerHit9 = audioClips["hit_9"])
        };

        playerForceFieldHit = audioClips["force_field_hit"];

        enemyHitClips = new BasicRandomList<AudioClip>
        {
            (enemyHit1 = audioClips["enemy_hit_1"]),
            (enemyHit2 = audioClips["enemy_hit_2"]),
            (enemyHit3 = audioClips["enemy_hit_3"])
        };

        explosionClips = new BasicRandomList<AudioClip>
        {
            (explosion1 = audioClips["explosion_1"]),
            (explosion2 = audioClips["explosion_2"]),
            (explosion3 = audioClips["explosion_3"])
        };

        pickupX2 = audioClips["pickup_x2"];
        pickupHealth = audioClips["pickup_health"];
        pickupShield = audioClips["pickup_shield"];

        musicIngameClips = new BasicRandomList<AudioClip>
        {
            (musicIngame1 = audioClips["InGame_TheAgeOfLoveJam"]),
            (musicIngame2 = audioClips["InGame_TechnoAXE - Spire_of_Light modified"])
        };

        uiConfirmClips = new BasicRandomList<AudioClip>
        {
            (uiConfirm1 = audioClips["ui_confirm_1"]),
            (uiConfirm2 = audioClips["ui_confirm_2"]),
            (uiConfirm3 = audioClips["ui_confirm_3"])
        };

        uiBackClips = new BasicRandomList<AudioClip>
        {
            (uiBack1 = audioClips["ui_back_1"]),
            (uiBack2 = audioClips["ui_back_2"])
        };

        uiConfirmNew = audioClips["ui_confirm_new"];
        uiBackNew = audioClips["ui_back_new"];
    }

    public void PlayUIButtonClip(UIButtonSoundType type)
    {
        player.PlayAndWait(
            type == UIButtonSoundType.Confirm ?
                uiConfirmNew :
                uiBackNew);

        //player.PlayAndWait(
        //    type == UIButtonSoundType.Confirm ?
        //        uiConfirmClips.Random :
        //        uiBackClips.Random);
    }
}