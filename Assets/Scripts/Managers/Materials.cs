﻿using UnityEngine;

public class Materials : UnitySingleton<Materials>
{
    [SerializeField] private ShipMaterialReadOnly m_enemyGlobal = null;
    [SerializeField] private ShipMaterialReadOnly m_enemyPassive = null;
    [SerializeField] private ShipMaterialReadOnly m_enemyGeneric = null;
    [SerializeField] private ShipMaterialReadOnly m_enemyFast = null;
    [SerializeField] private ShipMaterialReadOnly m_enemyDodger = null;
    [SerializeField] private ShipMaterialReadOnly m_enemyTough = null;

    //
    // Be warned that these properties can't be, their value can still be.
    // Consider creating a ReadonlyShipMaterial?
    //

    public ShipMaterialReadOnly EnemyGlobalHull => m_enemyGlobal;
    public ShipMaterialReadOnly EnemyPassive => m_enemyPassive;
    public ShipMaterialReadOnly EnemyGeneric => m_enemyGeneric;
    public ShipMaterialReadOnly EnemyFast => m_enemyFast;
    public ShipMaterialReadOnly EnemyDodger => m_enemyDodger;
    public ShipMaterialReadOnly EnemyTough => m_enemyTough;

    public ShipMaterialReadOnly GetEnemy(EnemyType type)
    {
        switch (type)
        {
            case EnemyType.Passive: return m_enemyPassive;
            case EnemyType.Generic: return m_enemyGeneric;
            case EnemyType.Fast:    return m_enemyFast;
            case EnemyType.Dodger:  return m_enemyDodger;
            case EnemyType.Tough:   return m_enemyTough;
            default: throw new System.ArgumentException("Invalid enemy type", nameof(type));
        }
    }
}