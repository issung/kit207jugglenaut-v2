﻿using UnityEngine;

public class GameSettings : UnitySingleton<GameSettings>
{
    public static bool SettingsLoaded { get; private set; }
    
    public Setting<bool> ShowFPS { get; private set; }

    public static Setting<ShipMaterial> EnemyPassive { get; private set; }
    public static Setting<ShipMaterial> EnemyGeneric { get; private set; }
    public static Setting<ShipMaterial> EnemyFast { get; private set; }
    public static Setting<ShipMaterial> EnemyDodger { get; private set; }
    public static Setting<ShipMaterial> EnemyTough { get; private set; }

    public override void Awake()
    {
        base.Awake();

        if (!SettingsLoaded)
        {
            ShowFPS = true;

            EnemyPassive = new Setting<ShipMaterial>(Materials.Instance.EnemyPassive);
            EnemyGeneric = new Setting<ShipMaterial>(Materials.Instance.EnemyGeneric);
            EnemyFast = new Setting<ShipMaterial>(Materials.Instance.EnemyFast);
            EnemyDodger = new Setting<ShipMaterial>(Materials.Instance.EnemyDodger);
            EnemyTough = new Setting<ShipMaterial>(Materials.Instance.EnemyTough);

            SettingsLoaded = true;
        }
    }

    public void UpdatePreviousSettings()
    {
        ShowFPS.UpdatePreviousValue();

        EnemyPassive.UpdatePreviousValue();
        EnemyGeneric.UpdatePreviousValue();
        EnemyFast.UpdatePreviousValue();
        EnemyDodger.UpdatePreviousValue();
        EnemyTough.UpdatePreviousValue();
    }

    public void ResetSettings()
    {
        ShowFPS.Reset();

        EnemyPassive.Reset();
        EnemyGeneric.Reset();
        EnemyFast.Reset();
        EnemyDodger.Reset();
        EnemyTough.Reset();
    }

    public ShipMaterial GetEnemyShipMaterial(EnemyType type)
    {
        switch (type)
        {
            case EnemyType.Passive: return EnemyPassive;
            case EnemyType.Generic: return EnemyGeneric;
            case EnemyType.Fast:    return EnemyFast;
            case EnemyType.Dodger:  return EnemyDodger;
            case EnemyType.Tough:   return EnemyTough;
            default: throw new System.ArgumentException("Invalid enemy type", nameof(type));
        }
    }
}