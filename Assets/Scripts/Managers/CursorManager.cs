﻿using UnityEngine;

public class CursorManager : UnitySingleton<CursorManager>
{
    public Texture2D cursor;

    public override void Awake()
    {
        base.Awake();
        CursorUtil.SetTexture(cursor, Vector2.zero);
    }
}