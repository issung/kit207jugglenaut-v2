﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioPlayer : MonoBehaviour
{
    [ShowOnly]
    public AudioSource audioSource;

    [ShowOnly]
    public AudioClip current;

    public float LastPlay { get; private set; }

    public float TimeLeft => current?.length - (Time.time - LastPlay) ?? -1f;

    public float MaxSoundsPerSecond = 0f;
    
    private float m_volume;
    private float m_pitch;

    public void Awake()
    {
        //audioSource = gameObject.GetComponent<AudioSource>();

        if (!audioSource)
            audioSource = gameObject.AddComponent<AudioSource>();

        LastPlay = Time.time;
    }

    /*private IEnumerator PlayAndWait0(AudioClip clip)
    {
        audioSource.PlayOneShot(current = clip);
        yield return new WaitForSecondsRealtime(clip.length);
    }*/

    private IEnumerator PlayAndWait0(AudioClip clip, Action post)
    {
        if (!clip)
        {
            System.Diagnostics.Debugger.Break();
            yield return null;
        }
        else if (MaxSoundsPerSecond > 0f ? Time.time - LastPlay > MaxSoundsPerSecond : true)
        {
            audioSource.PlayOneShot(current = clip);

            yield return new WaitForSecondsRealtime(clip.length);

            post?.Invoke();
        }

        LastPlay = Time.time;
    }

    public void PlayAndWait(AudioClip clip) => StartCoroutine(PlayAndWait0(clip, null));

    public void PlayAndWait(AudioClip clip, Action post) => StartCoroutine(PlayAndWait0(clip, post));

    public void Play(AudioClip clip, float volume = 1f, float pitch = 1f)
    {
        LastPlay = Time.time;
        SetVolumePitch(volume, pitch);
        audioSource.PlayOneShot(current = clip);
        //Reset();
    }

    public void SetVolume(float volume)
    {
        m_volume = audioSource.volume;
        audioSource.volume = volume;
    }

    public void ResetVolume() => audioSource.volume = m_volume;

    public void SetPitch(float pitch)
    {
        m_pitch = audioSource.pitch;
        audioSource.pitch = pitch;
    }

    public void ResetPitch() => audioSource.pitch = m_pitch;

    public void SetVolumePitch(float volume, float pitch)
    {
        SetVolume(volume);
        SetPitch(pitch);
    }

    private void ResetAudioPlayer()
    {
        ResetVolume();
        ResetPitch();
    }

    public override string ToString() => $"{audioSource?.name} ({TimeLeft}s left)";
}