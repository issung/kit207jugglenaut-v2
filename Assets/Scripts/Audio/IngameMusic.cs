﻿using System.Collections;
using UnityEngine;

public class IngameMusic : UnitySingleton<IngameMusic>
{
    private const int MAX_TRY_COUNT = 64;

    private AudioSource m_audioSource;

    [SerializeField] private bool m_debug = false;
    [ShowOnly, SerializeField] private AudioClip m_current;
    [ShowOnly, SerializeField] private AudioClip m_previous;

    private Coroutine m_playMusicCoroutine;

    [ShowOnly, SerializeField]
    [Tooltip("This is how many times the script tried to select an audio clip that wasn't the one that just played. Resets at the beginning of a new selection.")]
    private int m_tryCount;

    private float m_startTime;

    public override void Awake()
    {
        base.Awake();

        m_audioSource = gameObject.AddComponent<AudioSource>();
        m_audioSource.playOnAwake = false;
        m_audioSource.loop = false;
    }

    private void Start()
    {
        m_playMusicCoroutine = StartCoroutine(PlayMusicCoroutine());
    }

    private void OnDestroy() => StopMusic();

    public void StopMusic()
    {
        if (m_playMusicCoroutine != null)
            StopCoroutine(m_playMusicCoroutine);

        if (m_audioSource.isPlaying)
            m_audioSource.Stop();
    }

    public void StopMusicAndDestroy()
    {
        StopMusic();
        Destroy(gameObject);
    }

    private IEnumerator PlayMusicCoroutine()
    {
        while (true)
        {
            m_tryCount = 0;

            while (++m_tryCount < MAX_TRY_COUNT && m_current == m_previous)
                m_current = AudioManager.Instance.musicIngameClips.Random;

            m_previous = m_current;
            m_audioSource.PlayOneShot(m_current);
            m_startTime = Time.time;

            yield return new WaitForSeconds(m_current.length);
        }
    }

    private void OnGUI()
    {
        if (m_debug)
        {
            string text =
                $"Current Soundtrack: {m_current.name}\n" +
                $"Previous Soundtrack: {m_previous.name}\n" +
                $"Next track in ~{m_current.length - (Time.time - m_startTime):N0}s";

            Vector2 size = GUI.skin.box.CalcSize(new GUIContent(text));

            MyDebug.GuiAutoBox(Screen.width - size.x - 10, Screen.height - size.y - 10, text);
        }
    }
}