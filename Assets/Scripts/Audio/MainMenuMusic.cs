﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MainMenuMusic : UnitySingleton<MainMenuMusic>
{
    private AudioSource m_audioSource;

    public override void Awake()
    {
        base.Awake();
        m_audioSource = GetComponent<AudioSource>();
    }

    public void Stop()
    {
        if (m_audioSource.isPlaying)
            m_audioSource.Stop();
    }
}
