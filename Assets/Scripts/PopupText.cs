﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PopupText : MonoBehaviour
{
    public enum TextColor { Green, Orange, Red };

    private TextMeshPro tmp;
    private RectTransform rt;

    private bool shrink = false;

    void Awake()
    {
        tmp = GetComponent<TextMeshPro>();
        rt = GetComponent<RectTransform>();
    }

    public void Set(string text, TextColor textColor, Vector3 position)
    {
        Color color = new Color();
        TMP_FontAsset font = new TMP_FontAsset();

        switch (textColor)
        {
            case TextColor.Green:
                color = Color.green;
                font = PrefabLinks.Instance.BitDarlingGreen;
                break;
            case TextColor.Orange:
                color = new Color(255, 159, 0);
                font = PrefabLinks.Instance.BitDarlingOrange;
                break;
            case TextColor.Red:
                color = Color.red;
                font = PrefabLinks.Instance.BitDarlingRed;
                break;
        }

        gameObject.name = $"PopupText(\"{text}\")";
        tmp.text = text;
        tmp.color = color;
        tmp.outlineColor = color;
        tmp.font = font;
        tmp.fontSize = 6;
        var pos = position;
        pos.z = -1.5f;
        pos.x -= 3.5f;
        pos.x = Mathf.Clamp(pos.x, -11.3f, 4.6f);
        /*var pos = Camera.main.WorldToViewportPoint(position);
        pos.z = -3.5f;
        pos.x -= 1.5f;*/
        rt.position = pos;
        shrink = false;

        this.InvokeDelay(0.65f, () => { shrink = true; });
    }

    private void Update()
    {
        //t.position.SetY(t.position.y + (5000 * Controller.GetDtf()));
        rt.Translate(Vector3.up * 0.02f);

        if (shrink)
        {
            tmp.fontSize -= 0.3f;//tmp.fontSize * 0.1f;

            if (tmp.fontSize <= 0.1f)
            {
                Destroy(gameObject);
            }
        }

        /*Debug.Log($"rt: {rt.position.ToString()} shrink: {shrink}");*/
    }
}
