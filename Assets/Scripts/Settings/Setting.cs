﻿using System;

public class Setting<T>
{
    private T m_pValue;

    public T Value
    {
        get => m_pValue;
        set
        {
            //if (m_pValue == null ? true : !m_pValue.Equals(value))
            if (!Equals(m_pValue, value))
            {
                m_pValue = value;
                ValueChanged?.Invoke();
            }
        }
    }

    public T PreviousValue { get; set; }

    public event Action ValueChanged;

    public Setting(T value)
    {
        Value = value;
        UpdatePreviousValue();
    }

    public void UpdatePreviousValue() => PreviousValue = Value;

    public void Reset() => Value = PreviousValue;

    public void Set(T value) => Value = value;

    public static implicit operator T(Setting<T> setting) => setting.Value;
    public static implicit operator Setting<T>(T value) => new Setting<T>(value);
}