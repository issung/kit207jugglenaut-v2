﻿using UnityEngine;

public static class QuaternionExtensions
{
    public static Quaternion SetX(this Quaternion quaternion, float x) => Quaternion.Euler(x, quaternion.eulerAngles.y, quaternion.eulerAngles.z);
    public static Quaternion SetY(this Quaternion quaternion, float y) => Quaternion.Euler(quaternion.eulerAngles.x, y, quaternion.eulerAngles.z);
    public static Quaternion SetZ(this Quaternion quaternion, float z) => Quaternion.Euler(quaternion.eulerAngles.x, quaternion.eulerAngles.y, z);
}