﻿using UnityEditor;
using UnityEngine;

//Original version of the ConditionalEnableAttribute created by Brecht Lecluyse (www.brechtos.com)
//Modified by: Sebastian Lague

[CustomPropertyDrawer(typeof(ConditionalEnableAttribute))]
public class ConditionalEnablePropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        ConditionalEnableAttribute attr = (ConditionalEnableAttribute)attribute;
        bool enabled = GetConditionalEnableAttributeResult(attr, property);

        GUI.enabled = false;

        if (enabled == attr.Condition)
            GUI.enabled = true;

        EditorGUI.PropertyField(position, property, label, true);
    }
    
    private bool GetConditionalEnableAttributeResult(ConditionalEnableAttribute attribute, SerializedProperty property)
    {
        SerializedProperty sourcePropertyValue = null;

        //Get the full relative property path of the sourcefield so we can have nested hiding.Use old method when dealing with arrays
        if (!property.isArray)
        {
            string propertyPath = property.propertyPath; //returns the property path of the property we want to apply the attribute to
            string conditionPath = propertyPath.Replace(property.name, attribute.ConditionalSourceField); //changes the path to the conditionalsource property path
            sourcePropertyValue = property.serializedObject.FindProperty(conditionPath);

            //if the find failed->fall back to the old system
            if (sourcePropertyValue == null)
                //original implementation (doesn't work with nested serializedObjects)
                sourcePropertyValue = property.serializedObject.FindProperty(attribute.ConditionalSourceField);
        }
        else
            //original implementation (doesn't work with nested serializedObjects)
            sourcePropertyValue = property.serializedObject.FindProperty(attribute.ConditionalSourceField);

        if (sourcePropertyValue != null)
            return CheckPropertyType(attribute, sourcePropertyValue);

        return true;
    }

    private bool CheckPropertyType(ConditionalEnableAttribute attribute, SerializedProperty sourcePropertyValue)
    {
        //Note: add others for custom handling if desired
        switch (sourcePropertyValue.propertyType)
        {
            case SerializedPropertyType.Boolean:
                return sourcePropertyValue.boolValue;
            case SerializedPropertyType.Enum:
                return
                    attribute.Type == ConditionalAttributeTypes.EnumEqual ? sourcePropertyValue.enumValueIndex == attribute.EnumIndex :
                    attribute.Type == ConditionalAttributeTypes.EnumNotEqual ? sourcePropertyValue.enumValueIndex != attribute.EnumIndex :
                    throw new System.Exception($"ConditionalAttributeTypes can't be \"{attribute.Type}\" when \"propertyType\" is Enum.");
            default:
                Debug.LogError("Data type of the property used for conditional enabling [" + sourcePropertyValue.propertyType + "] is currently not supported");
                return true;
        }
    }
}