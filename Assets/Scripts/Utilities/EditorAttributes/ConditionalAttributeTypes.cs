﻿public enum ConditionalAttributeTypes
{
    Boolean,
    EnumEqual,
    EnumNotEqual
}