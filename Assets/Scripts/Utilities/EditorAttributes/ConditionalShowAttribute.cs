﻿using System;
using UnityEngine;

//Original version of the ConditionalHideAttribute created by Brecht Lecluyse (www.brechtos.com)
//Modified by: Sebastian Lague

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class | AttributeTargets.Struct, Inherited = true)]
public class ConditionalShowAttributeAttribute : PropertyAttribute
{
    public ConditionalAttributeTypes Type { get; }
    public string ConditionalSourceField { get; }
    public bool Condition { get; }
    public int EnumIndex { get; }

    public ConditionalShowAttributeAttribute(string conditionalSourceField, bool condition, int enumIndex, ConditionalAttributeTypes type)
    {
        Type = type;
        ConditionalSourceField = conditionalSourceField;
        Condition = condition;
        EnumIndex = enumIndex;
    }

    public ConditionalShowAttributeAttribute(string fieldName, bool condition) : this(fieldName, condition, -1, ConditionalAttributeTypes.Boolean) { }
}