﻿using System;
using UnityEngine;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class | AttributeTargets.Struct, Inherited = true)]
public class ConditionalEnableAttribute : PropertyAttribute
{
    public ConditionalAttributeTypes Type { get; }
    public string ConditionalSourceField { get; }
    public bool Condition { get; }
    public int EnumIndex { get; }

    public ConditionalEnableAttribute(string conditionalSourceField, bool condition, int enumIndex, ConditionalAttributeTypes type)
    {
        Type = type;
        ConditionalSourceField = conditionalSourceField;
        Condition = condition;
        EnumIndex = enumIndex;
    }

    public ConditionalEnableAttribute(string fieldName, bool condition) : this(fieldName, condition, -1, ConditionalAttributeTypes.Boolean) { }
}