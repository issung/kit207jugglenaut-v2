﻿using UnityEngine;

public static class CursorUtil
{
    public static void SetTexture(Texture2D texture) => Cursor.SetCursor(texture, new Vector2(texture.width / 2f, texture.height / 2f), CursorMode.ForceSoftware);
    public static void SetTexture(Texture2D texture, Vector2 hotspot) => Cursor.SetCursor(texture, hotspot, CursorMode.ForceSoftware);
}