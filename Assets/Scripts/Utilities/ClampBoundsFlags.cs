﻿[System.Flags]
public enum ClampBoundsFlags
{
    None = 0x1,
    IgnoreMinX = 0x2,
    IgnoreMinY = 0x4,
    IgnoreMinZ = 0x8,
    IgnoreMaxX = 0x10,
    IgnoreMaxY = 0x20,
    IgnoreMaxZ = 0x30,

    IgnoreX = IgnoreMinX | IgnoreMaxX,
    IgnoreY = IgnoreMinY | IgnoreMaxY,
    IgnoreZ = IgnoreMinZ | IgnoreMaxZ,
    IgnoreMin = IgnoreMinX | IgnoreMinY | IgnoreMinZ,
    IgnoreMax = IgnoreMaxX | IgnoreMaxY | IgnoreMaxZ,
}