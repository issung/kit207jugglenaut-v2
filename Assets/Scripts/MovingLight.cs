﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingLight : MonoBehaviour
{
    [SerializeField]
    private float m_speed = 20f;

    private float m_yRotationSart;
    private float m_yRotation;

    private void Awake()
    {
        m_yRotationSart = transform.GetYRotation();
        m_yRotation = m_yRotationSart;
    }

    private void Update()
    {
        m_yRotation += Controller.GetDtf() / m_speed;
        transform.SetXRotation(m_yRotation);
        transform.SetYRotation(m_yRotation);
    }
}
