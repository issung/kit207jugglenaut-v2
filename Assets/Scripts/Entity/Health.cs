﻿using System;
using UnityEngine;

public class Health : ProgressBar
{
    public override ProgressBarStartingValue StartValue => ProgressBarStartingValue.Maximum;

    public bool DestroyOnDeath = true;
    public bool CanReceiveDamage = true;

    public bool HasHealth => Value > 0f;
    public bool IsDead => Value <= 0f;

    private bool OnDeathCalled { get; set; }

    public event EventHandler Death;

    protected override void OnValueChanged()
    {
        if (IsDead)
        {
            if (!OnDeathCalled)
            {
                OnDeath();
                OnDeathCalled = true;
            }

            if (DestroyOnDeath)
                Destroy(gameObject);
                //Destroy(transform.parent.gameObject);
                //Destroy(transform.root.gameObject);
        }
    }
    
    protected virtual void OnDeath() => Death?.Invoke(this, EventArgs.Empty);
    //protected virtual void OnDeath() { }

    public virtual void AddHealth(float amount) => Value = Mathf.Min(Value + amount, Maximum);
    public virtual void RemoveHealth(float amount) => Value = Mathf.Max(Value - amount, 0);

    public virtual void ResetHealth()
    {
        OnDeathCalled = false;
        Value = Maximum;
    }
}