﻿using System.Collections;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    public static bool IsAnyShipInView { get; private set; }

    public abstract EnemyType EnemyType { get; }
    
    #pragma warning disable IDE0044, CS0649

    //[SerializeField] private int m_health;
    [SerializeField] protected float MoveSpeed;
    [SerializeField] protected float SpawnSpeed;

    // Dodger: 150
    // Fast: 125
    // Generic: 75
    // Tough: 160
    [SerializeField] protected int ScoreGain;

    [SerializeField] protected GameObject shipLightMesh;
    [SerializeField] protected GameObject healthBar;
    //[SerializeField] protected MeshRenderer thrusterLeft;
    //[SerializeField] protected MeshRenderer thrusterRight;

    [SerializeField] private GameObject m_smokingSparksInst;
    [SerializeField] private Light m_smokingSparksLight;

    #pragma warning restore IDE0044, CS0649

    protected AutoShoot autoShoot;
    protected new Rigidbody rigidbody;
    protected Health health;

    protected bool IsDying { get; private set; }
    protected bool IsInView { get; private set; }

    private ParticleSystem m_smokingSparks;

    protected virtual void Awake()
    {
        autoShoot = GetComponent<AutoShoot>();

        if (autoShoot)
            autoShoot.shouldShoot = false;

        rigidbody = GetComponent<Rigidbody>();
        health = GetComponentInChildren<Health>();
        health.Death += OnDeath;
        health.DestroyOnDeath = false;

        WaveSpawner.Instance.enemiesSpawned++;

        m_smokingSparks = m_smokingSparksInst.GetComponent<ParticleSystem>();

        //m_smokingSparksInst = ParticleManager.Instance.CreateSmokingSparks(out m_smokingSparks);
        //m_smokingSparksInst.transform.SetParent(transform);
        //m_smokingSparksInst.transform.localPosition = Vector3.zero;

        IsInView = false;
    }

    /*private void OnBecameVisible() => IsAnyShipInView = true;

    private void OnBecameInvisible()
    {
        if (WaveSpawner.Instance.enemiesSpawned == 0)
            IsAnyShipInView = false;
    }*/

    protected virtual void Update()
    {
        //if (!HUD.Instance.HasGameEnded && gameObject.transform.GetX() > Controller.Instance.EnemyStopX)
        if (!HUD.Instance.HasGameEnded && !IsDying)
        {
            gameObject.transform.position += Vector3.left * (!IsInView ? SpawnSpeed : MoveSpeed) * Time.deltaTime;
            Controller.Instance.ClampTransformToGameBounds(gameObject.transform, IsInView ? ClampBoundsFlags.None : ClampBoundsFlags.IgnoreMaxX);
        }

        if (!IsInView && transform.GetX() < Controller.Instance.GameBounds.max.x)
        {
            if (autoShoot)
                autoShoot.shouldShoot = true;

            IsAnyShipInView = true;
            IsInView = true;
        }

        if (transform.GetX() <= Controller.Instance.GameBounds.min.x && !IsDying)
        {
            WaveSpawner.Instance.enemiesLeft--;

            if (--WaveSpawner.Instance.enemiesSpawned <= 0)
                IsAnyShipInView = false;

            WaveSpawner.Instance.UpdateSpawner();
            WaveSpawner.Instance.Spawned.Remove(gameObject);
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground" && IsDying)
        {
            rigidbody.velocity += Vector3.left * 7.5f;
            //transform.SetParent(collision.transform, true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PlayerBullet")
        {
            Bullet bullet = other.gameObject.GetComponent<Bullet>();
            health.RemoveHealth(bullet.damage);
            bullet.HitSomething(BulletHitBehaviour.Queue);
            AudioManager.Instance.enemyHitPlayer.SetPitch(Random.Range(0.8f, 1f));
            AudioManager.Instance.enemyHitPlayer.PlayAndWait(AudioManager.Instance.enemyHitClips.Random);

            //rigidbody.AddForce(bullet.lastFrameDirection * bullet.knockback);
            gameObject.transform.position += Vector3.right * bullet.knockback;
            //Debug.Log($"hit by player, knockback: {bullet.knockback}");
        }
    }

    public void Kill() => health.RemoveHealth(health.Value);

    private void OnDeath(object sender, System.EventArgs e)
    {
        AudioManager.Instance.explosionPlayer.PlayAndWait(AudioManager.Instance.explosionClips.Random);
        Player.Score += ScoreGain;

        IsDying = true;
        rigidbody.useGravity = true;
        rigidbody.drag = 0f;
        rigidbody.freezeRotation = false;
        rigidbody.AddForce(-0.4f, 0f, 0f, ForceMode.Impulse);
        rigidbody.AddTorque(Random.onUnitSphere.normalized * 20f, ForceMode.Impulse);

        if (autoShoot)
            autoShoot.StopShooting();

        shipLightMesh.gameObject.SetActive(false);
        healthBar.SetActive(false);

        ParticleManager.Instance.SpawnPlasmaExplosion(transform.position);
        m_smokingSparksLight.enabled = true;
        m_smokingSparks.Play();

        WaveSpawner.Instance.Spawned.Remove(gameObject);
        WaveSpawner.Instance.enemiesSpawned--;
        WaveSpawner.Instance.enemiesLeft--;
        WaveSpawner.Instance.UpdateSpawner();

        //StartCoroutine(ThrusterFlicker(thrusterLeft));
        //StartCoroutine(ThrusterFlicker(thrusterRight));

        PopupTextController.Popup($"+{ScoreGain}", PopupText.TextColor.Orange, transform.position);

        StartCoroutine(DelayedDeactivate());
    }

    private IEnumerator DelayedDeactivate()
    {
        yield return new WaitForSeconds(10f);

        LeanTween.value(gameObject, (Vector3 size) => { transform.localScale = size; }, transform.localScale, Vector3.zero, 2f).setOnComplete(() =>
        {
            IsDying = false;
            //gameObject.transform.root.gameObject.SetActive(false);
            Destroy(gameObject);
        });
    }

    /*private IEnumerator ThrusterFlicker(MeshRenderer thruster)
    {
        while (true)
        {
            thruster.enabled = false;

            yield return new WaitForSeconds(Random.Range(0.2f, 0.35f));

            thruster.enabled = true;
        }
    }*/

    //private void OnGUI() => MyDebug.GuiBoxObj(transform, $"{EnemyType}");
}
