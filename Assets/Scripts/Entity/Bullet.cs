﻿using System.Collections;
using UnityEngine;

public partial class Bullet : MonoBehaviour
{
    public enum Type
    {
        Direction, Homing
    }

    public Type type;
    public BulletHitBehaviour hitBehaviour;
    public bool friendly = false;
    public float speed = 1f;
    public Material material;

    public float damage = 0;     // Amount of damage this bullet will do to the object that gets hit(?)
    public float knockback = 0f; // Amount of knockback to give on object that gets hit(?)

    protected MeshRenderer mr;
    protected TrailRenderer tr;
    protected Rigidbody rb;

    public Vector3 lastFramePosition = Vector3.zero;
    public Vector3 lastFrameDirection = Vector3.zero;

    protected bool hit = false;

    Coroutine m_poolCoroutine = null;

    void Awake()
    {
        mr = GetComponent<MeshRenderer>();
        tr = GetComponent<TrailRenderer>();
        rb = GetComponent<Rigidbody>();
        Awake2();
    }

    public virtual void Awake2()
    {
        // override this in an extending class.
    }

    public void Update()
    {
        // Disable and queue bullets if they shoot off the right hand side of the screen
        if (transform.position.x > Controller.Instance.GameBounds.max.x ||
            transform.position.x < Controller.Instance.GameBounds.min.x)
            StopAndQueueBullet();

        lastFrameDirection = transform.position - lastFramePosition;
        lastFramePosition = transform.position;
    }

    public void SetVisible(bool visible)
    {
        mr.enabled = visible;
    }

    public void HitSomething(BulletHitBehaviour behaviour)
    {
        ParticleManager.Instance.SpawnSparks(transform.position);

        switch (hitBehaviour = behaviour)
        {
            case BulletHitBehaviour.Queue:
                StopAndQueueBullet();
                break;
            case BulletHitBehaviour.BounceQueue:
                StartCoroutine(Bounce());
                break;
            default:
                break;
        }
    }

    private void StopAndQueueBullet()
    {
        hit = true;
        speed = 0;
        rb.velocity = Vector3.zero;
        m_poolCoroutine = this.InvokeDelay(0.1f, () =>
        {
            Controller.QueueBullet(this);
        });
    }

    protected IEnumerator TimeOut()
    {
        yield return new WaitForSeconds(speed * 0.25f + 2);
        Controller.QueueBullet(this);
    }

    protected IEnumerator Bounce()
    {
        hit = true;
        rb.useGravity = true;
        rb.velocity = (-lastFrameDirection * 6) + (Vector3.down * 5);

        yield return new WaitForSeconds(Random.Range(4f, 5f));

        rb.useGravity = false;
        hit = false;
        Controller.QueueBullet(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        //print("OnTriggerEnter -bullet");

        //StopAndQueueBullet();
    }
}
