﻿public class EnemyPassive : Enemy
{
    public override EnemyType EnemyType => EnemyType.Passive;
}