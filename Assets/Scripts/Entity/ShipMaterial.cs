﻿using System;
using UnityEngine;

[Serializable]
public class ShipMaterial
{
    //[SerializeField] public Material m_hullVariant1;
    //[SerializeField] public Material m_hullVariant2;
    [SerializeField] public Material m_hull;
    [SerializeField] public Material m_glowPlane;

    //public Material HullVariant1 => m_hullVariant1;
    //public Material HullVariant2 => m_hullVariant2;

    public Material Hull
    {
        get => m_hull;
        set => m_hull = value;
    }

    public Material GlowPlane
    {
        get => m_glowPlane;
        set => m_glowPlane = value;
    }

    public ShipMaterial(Material hull, Material glowPlane)
    {
        m_hull = hull ?? throw new ArgumentNullException(nameof(hull));
        m_glowPlane = glowPlane; //?? throw new ArgumentNullException(nameof(glowPlane));
    }

    public void SetHull(Material material) => Hull = material;

    public ShipMaterial Clone() => new ShipMaterial(m_hull, m_glowPlane);

    public static implicit operator ShipMaterialReadOnly(ShipMaterial material) => new ShipMaterialReadOnly(material.m_hull, material.m_glowPlane);
}