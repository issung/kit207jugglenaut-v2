﻿using UnityEngine;

public class EnemyShipMaterial : MonoBehaviour
{
    [SerializeField] private GameObject m_glowPlane = null;

    [Header("Body")]
    [SerializeField] private GameObject m_bodyMain = null;
    [SerializeField] private GameObject m_bodyLower = null;
    [SerializeField] private GameObject m_bodySides = null;

    [Header("Gun")]
    [SerializeField] private GameObject m_gunPart1 = null;
    [SerializeField] private GameObject m_gunPart2 = null;
    [SerializeField] private GameObject m_gun = null;

    [SerializeField] private bool m_grabEnemyTypeFromEnemyScript = true;

    [ConditionalShowAttribute(nameof(m_grabEnemyTypeFromEnemyScript), false)]
    [SerializeField] private EnemyType m_enemyType;

    private Enemy m_enemy;

    private MeshRenderer m_glowPlane_MR;

    private MeshRenderer m_bodyMain_MR;
    private MeshRenderer m_bodyLower_MR;
    private MeshRenderer m_bodySides_MR;
    private MeshRenderer m_gunPart1_MR;
    private MeshRenderer m_gunPart2_MR;
    private MeshRenderer m_gun_MR;

    /*public MeshRenderer BodyMain
    {
        get => m_bodyMain;
        set => m_bodyMain = value;
    }

    public MeshRenderer BodyLower
    {
        get => m_bodyLower;
        set => m_bodyLower = value;
    }

    public MeshRenderer BodySides
    {
        get => m_bodySides;
        set => m_bodySides = value;
    }

    public MeshRenderer GunPart1
    {
        get => m_gunPart1;
        set => m_gunPart1 = value;
    }

    public MeshRenderer GunPart2
    {
        get => m_gunPart2;
        set => m_gunPart2 = value;
    }

    public MeshRenderer Gun
    {
        get => m_gun;
        set => m_gun = value;
    }*/
    
    public EnemyType EnemyType
    {
        get => m_enemyType;
        set => m_enemyType = value;
    }

    public ShipMaterial ShipMaterial { get; private set; }

    private void Awake()
    {
        m_enemy = GetComponent<Enemy>();

        if (m_grabEnemyTypeFromEnemyScript)
        {
            if (m_enemy)
                EnemyType = m_enemy.EnemyType;
            else
                Debug.LogError($"Failed to retrieve Enemy component for {gameObject.name}: The component could not be found.");
        }

        m_glowPlane_MR = m_glowPlane.GetComponent<MeshRenderer>();

        m_bodyMain_MR = m_bodyMain.GetComponent<MeshRenderer>();
        m_bodyLower_MR = m_bodyLower.GetComponent<MeshRenderer>();
        m_bodySides_MR = m_bodySides.GetComponent<MeshRenderer>();

        m_gun_MR = m_gun.GetComponent<MeshRenderer>();
        m_gunPart1_MR = m_gunPart1.GetComponent<MeshRenderer>();
        m_gunPart2_MR = m_gunPart2.GetComponent<MeshRenderer>();

        ReloadShipMaterials();
    }

    public void SetShipMaterials(ShipMaterial material)
    {
        ShipMaterial = material;

        if (material.Hull)
        {
            m_bodyMain_MR.material =
            m_bodyLower_MR.material =
            m_bodySides_MR.material =
            m_gun_MR.material =
            m_gunPart1_MR.material =
            m_gunPart2_MR.material = material.Hull;
        }

        if (material.GlowPlane)
        {
            m_glowPlane_MR.material = material.GlowPlane;
        }

        if (EnemyType == EnemyType.Passive)
        {
            m_gun.SetActive(false);
            m_gunPart2.SetActive(false);
        }
        else
        {
            m_gun.SetActive(true);
            m_gunPart2.SetActive(true);
        }
    }

    public void ReloadShipMaterials() => SetShipMaterials(GameSettings.Instance.GetEnemyShipMaterial(EnemyType));
}