﻿using System;
using UnityEngine;

public class Shield : ProgressBar
{
    public override ProgressBarStartingValue StartValue => ProgressBarStartingValue.Maximum;
    
    public bool canReceiveDamage = true;

    public bool IsShielded => Value > 0f;
    public bool IsShieldDepleted => Value <= 0f;

    private bool OnShieldDeactivatedCalled { get; set; }

    public event EventHandler ShieldDeactivated;

    protected override void OnValueChanged()
    {
        if (IsShieldDepleted)
        {
            if (!OnShieldDeactivatedCalled)
            {
                OnShieldDeactivated();
                OnShieldDeactivatedCalled = true;
                //gameObject.SetActive(false);
            }
        }
    }
    
    protected virtual void OnShieldDeactivated() => ShieldDeactivated?.Invoke(this, EventArgs.Empty);
    //protected virtual void OnShieldDeactivated() { }

    public virtual void AddStrength(float amount) => Value = Mathf.Min(Value + amount, Maximum);
    public virtual void RemoveStrength(float amount) => Value = Mathf.Max(Value - amount, 0);

    public virtual void ResetShield()
    {
        OnShieldDeactivatedCalled = false;
        Value = Maximum;
    }
}