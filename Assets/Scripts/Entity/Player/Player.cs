﻿using System.Collections;
using UnityEngine;

/*
 * This script is mostly based on "Jayanam Games": https://www.patreon.com/posts/unity-3d-drag-22917454.
 */
public class Player : MonoBehaviour
{
    private static int m_score;

    private static bool m_debugDragging = false;
    private static bool m_firstDrag = true;
    private static Vector3 m_firstDragPosition;
    private static bool m_firstDragPositionSet = false;
    private static Transform m_dragginPlayer;

    private static float FirstDragDistance => (m_firstDragPosition - m_dragginPlayer.position).magnitude;
    private static bool FirstDragComplete => FirstDragDistance >= 1f;

    public static int Score
    {
        get => m_score;
        set
        {
            if (m_score != value)
            {
                m_score = value;
                HUD.Instance.UpdateScore(value);
            }
        }
    }

    public AutoShoot AutoShoot { get; private set; }

    public float ShieldIntegrity { get; set; }

    public bool IsDead => m_health.ProgressBar.IsDead;

    public bool IsShielded => ShieldIntegrity > 0f;
    
    public bool CanPickupHealthPack => m_health.ProgressBar.Fraction < 1f;

    public bool CanPickupShield => !IsShielded;

    private GameObject m_shipOutline;
    private MeshRenderer[] m_shipOutlineMeshRenderers;
    
    #pragma warning disable CS0649

    [SerializeField, ColorUsage(false, true)] private Color m_shipOutlineHurtColour;
    [SerializeField, ColorUsage(false, true)] private Color m_shipOutlineShieldColour;
    [SerializeField, ColorUsage(false, true)] private Color m_shipOutlineShieldHitColour;

    #pragma warning restore CS0649

    //private Color m_shipOutlinePreviousColour;
    //private bool m_shipOutlinePreviousActive;

    private Vector3 m_offset;
    private float m_zCoord;
    private Rigidbody m_rigidbody;

    #pragma warning disable IDE0044, CS0649

    //Defaults are x=11.2 & y=6
    [SerializeField] private float m_xRange;
    //[SerializeField] private float m_yMin, m_yMax;

    [Header("Health")]
    [SerializeField] private HealthDisplay m_health;
    [SerializeField] private HealthBar m_healthbar;

    [Header("Shields")]
    [SerializeField] private ShieldDisplay m_shield;
    [SerializeField] private float m_shieldHitPitchMin = 2f;
    [SerializeField] private float m_shieldHitPitchMax = 2.25f;

    [Header("Particles")]
    [SerializeField] private GameObject m_smokingSparksInst;
    [SerializeField] private Light m_smokingSparksLight;

    private ParticleSystem m_smokingSparks;

    private WaitForSeconds m_playerUpdateInterval;
    private Coroutine m_playerUpdateCoroutine;
    private Coroutine m_flashHurtAlertCoroutine;

    #pragma warning restore IDE0044, CS0649

    private bool m_dragging = false;

    private bool IsThisDraggingPlayer => transform.Equals(m_dragginPlayer);

    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        AutoShoot = GetComponent<AutoShoot>();
        m_shipOutline = transform.Find("HurtGlowAlert").gameObject;
        m_shipOutlineMeshRenderers = m_shipOutline.GetComponentsInChildren<MeshRenderer>();

        for (int i = 0; i < m_shipOutlineMeshRenderers.Length; i++)
            m_shipOutlineMeshRenderers[i].material.EnableKeyword("_EMISSION");

        //m_shipOutlinePreviousColour = m_shipOutlineMeshRenderers[0].material.GetColor("_EmissionColor");

        m_playerUpdateInterval = new WaitForSeconds(0.125f);
        m_playerUpdateCoroutine = StartCoroutine(PlayerUpdateCoroutine());

        m_smokingSparks = m_smokingSparksInst.GetComponent<ParticleSystem>();

    }

    private void Start()
    {
        m_shield.gameObject.SetActive(false);
        m_health.ProgressBar.Death += OnDeath;

        if (!m_firstDrag)
            Controller.Instance.StartGame();
    }

    private void OnDestroy()
    {
        if (m_playerUpdateCoroutine != null)
            StopCoroutine(m_playerUpdateCoroutine);

        if (m_flashHurtAlertCoroutine != null)
            StopCoroutine(m_flashHurtAlertCoroutine);
    }

    private void OnMouseDown()
    {
        m_zCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;

        //m_rigidbody.position = transform.position;

        // Store offset = gameobject world pos - mouse world pos
        m_offset = gameObject.transform.position - GetMouseAsWorldPoint();

        m_dragging = true;

        if (!m_firstDragPositionSet)
        {
            m_dragginPlayer = transform;
            m_firstDragPosition = m_dragginPlayer.position;
            m_firstDragPositionSet = true;
        }
    }

    private void OnMouseUp()
    {
        m_dragging = false;

        if (!FirstDragComplete)
        {
            m_firstDragPositionSet = false;
            m_dragginPlayer = null;
        }
    }

    /*void OnMouseDrag()
    {
        //rb.position = GetMouseAsWorldPoint() + mOffset;

        //rb.position = new Vector3(
        //    Mathf.Clamp(rb.position.x, -xRange, xRange),
        //    Mathf.Clamp(rb.position.y, -yRange, yRange),
        //    rb.position.z);

        transform.position = GetMouseAsWorldPoint() + mOffset;

        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, -xRange, xRange),
            Mathf.Clamp(transform.position.y, -yRange, yRange),
            transform.position.z);
    }*/

    private void Update()
    {
        if (m_health.ProgressBar.Fraction <= 0.2f && !m_smokingSparks.isPlaying)
        {
            m_smokingSparks.Play();
            m_smokingSparksLight.enabled = true;
        }

        if (m_dragging && !HUD.Instance.HasGameEnded)
        {
            /*m_rigidbody.position = GetMouseAsWorldPoint() + m_offset;

            m_rigidbody.position = new Vector3(
                Mathf.Clamp(m_rigidbody.position.x, -m_xRange, m_xRange),
                Mathf.Clamp(m_rigidbody.position.y, -m_yRange, m_yRange),
                m_rigidbody.position.z);*/

            transform.position = GetMouseAsWorldPoint() + m_offset;

            transform.position = new Vector3(
                Mathf.Clamp(transform.position.x, -m_xRange, m_xRange),
                Mathf.Clamp(transform.position.y, Controller.Instance.GameBounds.min.y, CalcMaxY()), 0f);

            if (m_firstDrag && FirstDragComplete)
            {
                m_firstDrag = false;
                Controller.Instance.StartGame();
            }

            if (m_debugDragging && IsThisDraggingPlayer && m_firstDrag)
                Debug.DrawLine(m_firstDragPosition, m_dragginPlayer.position);
        }

        // This should be moved to be an event based code.
        AutoShoot.shouldShoot = WaveSpawner.Instance.enemiesSpawned > 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EnemyBullet")
        {
            Bullet bullet = other.gameObject.GetComponent<Bullet>();
            Hit(bullet.damage);
            bullet.HitSomething(IsShielded ? BulletHitBehaviour.BounceQueue : BulletHitBehaviour.Queue);

            m_rigidbody.AddForce(bullet.lastFrameDirection * bullet.knockback);
        }
    }

    private void OnDeath(object sender, System.EventArgs e)
    {
        AudioManager.Instance.explosionPlayer.PlayAndWait(AudioManager.Instance.explosionClips.Random);
        HUD.Instance.SetGameEnded(false);
    }

    internal void AddHealth(float add)
    {
        m_health.ProgressBar.Value += add;
        m_healthbar.SetHealth(m_health.ProgressBar.Value);
        //m_healthDisplay.health = m_health;

        if (m_health.ProgressBar.Fraction > 0.2f && m_smokingSparks.isPlaying)
        {
            m_smokingSparks.Stop();
            m_smokingSparksLight.enabled = false;
        }
    }

    private IEnumerator PlayerUpdateCoroutine()
    {
        while (true)
        {
            Utils.ClampTransformToBounds(gameObject.transform, -m_xRange, Controller.Instance.GameBounds.min.y, 0f, m_xRange, CalcMaxY(), 0f);
            //Utils.ClampTransformToBounds(gameObject.transform, -m_xRange, m_yMin, 0f, m_xRange, m_yMax, 0f);

            yield return m_playerUpdateInterval;
        }
    }

    private Vector3 GetMouseAsWorldPoint()
    {
        // Pixel coordinates of mouse (x,y)
        Vector3 mousePoint = Input.mousePosition;

        // z coordinate of game object on screen
        mousePoint.z = m_zCoord;

        // Convert it to world points
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    private void Hit(float damage)
    {
        if (IsShielded)
        {
            if (ShieldIntegrity - damage <= 0f)
            {
                m_health.ProgressBar.Value -= damage - ShieldIntegrity;
                m_healthbar.SetHealth(m_health.ProgressBar.Value);

                ShieldIntegrity = 0f;
                m_shield.gameObject.SetActive(false);

                SetShipOutlineColour(m_shipOutlineHurtColour);

            }
            else
            {
                ShieldIntegrity -= damage;
                m_shield.ProgressBar.Value = ShieldIntegrity;
            }

            StopCoroutine(FlashHurtAlert());
            StartCoroutine(FlashHurtAlert());

            AudioManager.Instance.player.SetVolumePitch(1f, Random.Range(m_shieldHitPitchMin, m_shieldHitPitchMax));
            AudioManager.Instance.player.PlayAndWait(AudioManager.Instance.playerForceFieldHit);
        }
        else
        {
            // Subtract health
            m_health.ProgressBar.Value -= damage;

            // Tell health bar new health value.
            m_healthbar.SetHealth(m_health.ProgressBar.Value);

            // Camera effects.
            LeanTween.value(gameObject, (float i) =>
            {
                CameraEffectsController.SetEffect(CameraEffectsController.Effect.ColorDrift, i);
                CameraEffectsController.SetEffect(CameraEffectsController.Effect.ScanLineJitter, i);
            }, 0.05f * 2, 0f, 1f);

            LeanTween.value(gameObject, (float i) =>
            {
                CameraEffectsController.SetEffect(CameraEffectsController.Effect.DigitalGlitch, i);
            }, 0.025f * 2, 0f, 0.5f);

            AudioManager.Instance.player.PlayAndWait(AudioManager.Instance.playerHitClips.Random);

            transform.position += Vector3.left * 0.2f;
            m_offset += Vector3.left * 0.1f;

            PopupTextController.Popup($"-{damage} HP", PopupText.TextColor.Red, transform.position);

            StopCoroutine(FlashHurtAlert());
            StartCoroutine(FlashHurtAlert());
        }
    }

    private IEnumerator FlashHurtAlert()
    {
        const float ontime = 0.15f, offtime = 0.075f;

        // Track current state.
        //m_shipOutlinePreviousActive = m_shipOutline.activeSelf;

        if (IsShielded)
        {
            m_shipOutline.SetActive(true);
            SetShipOutlineColour(m_shipOutlineShieldHitColour);
            yield return new WaitForSeconds(0.6f);
            SetShipOutlineColour(m_shipOutlineShieldColour);
        }
        else
        {
            SetShipOutlineColour(m_shipOutlineHurtColour);

            m_shipOutline.SetActive(true);
            yield return new WaitForSeconds(ontime);    //flash on for ontime seconds
            m_shipOutline.SetActive(false);
            yield return new WaitForSeconds(offtime);   //flash off for offtime seconds
            m_shipOutline.SetActive(true);
            yield return new WaitForSeconds(ontime);    //flash on for ontime seconds
            m_shipOutline.SetActive(false);
            yield return new WaitForSeconds(offtime);   //flash off for offtime seconds
            m_shipOutline.SetActive(true);
            yield return new WaitForSeconds(ontime);    //flash on for ontime seconds
        }

        //RestoreShipOutlineColour();

        // Doesn't matter if the player has no shields because the outline will be hidden accordingly.
        SetShipOutlineColour(m_shipOutlineShieldColour);

        // Return to previous state. 
        //m_shipOutline.SetActive(m_shipOutlinePreviousActive);
        m_shipOutline.SetActive(IsShielded);
    }

    private void SetShipOutlineColour(Color colour)
    {
        //m_shipOutlinePreviousColour = m_shipOutlineMeshRenderers[0].material.GetColor("_EmissionColor");

        for (int i = 0; i < m_shipOutlineMeshRenderers.Length; i++)
            m_shipOutlineMeshRenderers[i].material.SetColor("_EmissionColor", colour);
    }

    //private void RestoreShipOutlineColour() => SetShipOutlineColour(m_shipOutlinePreviousColour);

    public void ActivateShields()
    {
        ShieldIntegrity = m_shield.ProgressBar.Maximum;
        m_shield.ProgressBar.Value = ShieldIntegrity;

        SetShipOutlineColour(m_shipOutlineShieldColour);
        m_shipOutline.SetActive(true);
        m_shield.gameObject.SetActive(true);
    }

    private void OnGUI()
    {
        if (m_debugDragging && IsThisDraggingPlayer && m_firstDrag)
            MyDebug.GuiBoxObj(transform.position, Vector3.up * 2f, $"Dragged {FirstDragDistance:N1}m");
    }

    private static float CalcMaxY() => Controller.Instance.GameBounds.max.y;// - 0.75f;

    public static void ResetStaticProperties()
    {
        //m_firstDrag = true;
        //m_dragginPlayer = null;
        //m_firstDragPositionSet = false;
        //m_firstDragPosition = Vector3.zero;
        Score = 0;
    }
}