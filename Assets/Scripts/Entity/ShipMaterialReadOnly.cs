﻿using System;
using UnityEngine;

[Serializable]
public class ShipMaterialReadOnly
{
    [SerializeField] public Material m_hull;
    [SerializeField] public Material m_glowPlane;

    public Material Hull => m_hull;

    public Material GlowPlane => m_glowPlane;

    public ShipMaterialReadOnly(Material hull, Material glowPlane)
    {
        m_hull = hull ?? throw new ArgumentNullException(nameof(hull));
        m_glowPlane = glowPlane ?? throw new ArgumentNullException(nameof(glowPlane));
    }

    public static implicit operator ShipMaterial(ShipMaterialReadOnly material) => new ShipMaterial(material.m_hull, material.m_glowPlane);
}