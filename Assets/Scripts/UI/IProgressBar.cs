﻿public interface IProgressBar
{
    float Minimum { get; }
    float Maximum { get; }
    float Value { get; }

    float Fraction { get; }
}