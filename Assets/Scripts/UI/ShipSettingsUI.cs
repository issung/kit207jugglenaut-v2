﻿using System;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ShipSettingsUI : MonoBehaviour
{
    [Header("Model")]
    [SerializeField] private EnemyShipMaterial m_enemyShipDisplay = null;
    [ShowOnly, SerializeField] private int m_currentShipIndex = 0;
    [ShowOnly, SerializeField] private int m_maxShips = -1;

    [Header("Settings Controls")]
    [SerializeField] private Button m_buttonSave = null;
    [SerializeField] private Button m_buttonCancel = null;

    [Header("Ship Controls")]
    [SerializeField] private Button m_buttonPreviousShip = null;
    [SerializeField] private Button m_buttonNextShip = null;

    [Header("Ship Settings Controls")]
    [SerializeField] private TextMeshProUGUI m_shipName = null;
    [SerializeField] private Button m_buttonDefaultHull = null;
    [SerializeField] private Button m_buttonGreyHull = null;

    private Material m_enemyPassive;
    private Material m_enemyGeneric;
    private Material m_enemyFast;
    private Material m_enemyDodger;
    private Material m_enemyTough;

    private void Awake()
    {
        m_maxShips = Enum.GetValues(typeof(EnemyType)).Length;

        m_buttonSave.onClick.AddListener(OnSave);
        m_buttonCancel.onClick.AddListener(OnCancel);

        m_buttonPreviousShip.onClick.AddListener(OnPrevShip);
        m_buttonNextShip.onClick.AddListener(OnNextShip);

        m_buttonDefaultHull.onClick.AddListener(OnHullDefaultClicked);
        m_buttonGreyHull.onClick.AddListener(OnHullGreyClicked);

        m_currentShipIndex = 0;
        SetActiveShip(m_currentShipIndex);
    }

    private void SetActiveShip(int activeShipIndex)
    {
        m_enemyShipDisplay.EnemyType = (EnemyType)m_currentShipIndex;
        m_enemyShipDisplay.ReloadShipMaterials();
        m_shipName.text = m_enemyShipDisplay.EnemyType.ToString();
    }

    private void OnSave()
    {
        GameSettings.EnemyPassive.Value.Hull = m_enemyPassive;
        GameSettings.EnemyGeneric.Value.Hull = m_enemyGeneric;
        GameSettings.EnemyFast.Value.Hull = m_enemyFast;
        GameSettings.EnemyDodger.Value.Hull = m_enemyDodger;
        GameSettings.EnemyTough.Value.Hull = m_enemyTough;

        SceneManager.LoadScene("MainMenu");
        AudioManager.Instance.PlayUIButtonClip(UIButtonSoundType.Confirm);
    }

    private void OnCancel()
    {
        SceneManager.LoadScene("MainMenu");
        AudioManager.Instance.PlayUIButtonClip(UIButtonSoundType.BackOrCancel);
    }

    private void OnPrevShip()
    {
        if (--m_currentShipIndex < 0)
            m_currentShipIndex = m_maxShips - 1;

        AudioManager.Instance.PlayUIButtonClip(UIButtonSoundType.Confirm);
        SetActiveShip(m_currentShipIndex);
    }

    private void OnNextShip()
    {
        if (++m_currentShipIndex > m_maxShips - 1)
            m_currentShipIndex = 0;

        AudioManager.Instance.PlayUIButtonClip(UIButtonSoundType.Confirm);
        SetActiveShip(m_currentShipIndex);
    }

    private void OnHullDefaultClicked()
    {
        m_enemyShipDisplay.SetShipMaterials(Materials.Instance.GetEnemy(m_enemyShipDisplay.EnemyType));
        FindChangedSetting();
        AudioManager.Instance.PlayUIButtonClip(UIButtonSoundType.Confirm);
    }

    private void OnHullGreyClicked()
    {
        try
        {
            m_enemyShipDisplay.SetShipMaterials(Materials.Instance.EnemyGlobalHull);
            FindChangedSetting();
        }
        catch (Exception ex)
        {
            File.WriteAllText("ERROR.txt", $"Error:\n{ex.Message}\n\n{ex.StackTrace}");
        }

        AudioManager.Instance.PlayUIButtonClip(UIButtonSoundType.Confirm);
    }

    private void FindChangedSetting()
    {
        switch (m_enemyShipDisplay.EnemyType)
        {
            case EnemyType.Passive:
                m_enemyPassive = m_enemyShipDisplay.ShipMaterial.Hull;
                break;
            case EnemyType.Generic:
                m_enemyGeneric = m_enemyShipDisplay.ShipMaterial.Hull;
                break;
            case EnemyType.Fast:
                m_enemyFast = m_enemyShipDisplay.ShipMaterial.Hull;
                break;
            case EnemyType.Dodger:
                m_enemyDodger = m_enemyShipDisplay.ShipMaterial.Hull;
                break;
            case EnemyType.Tough:
                m_enemyTough = m_enemyShipDisplay.ShipMaterial.Hull;
                break;
            default:
                throw new ArgumentException("Invalid enemy type", nameof(m_enemyShipDisplay.EnemyType));
        }
    }
}