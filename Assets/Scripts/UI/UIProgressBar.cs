﻿using UnityEngine;

public abstract class UIProgressBar<T> : MonoBehaviour where T : ProgressBar
{
    [ShowOnly, SerializeField] public float width;
    [ShowOnly, SerializeField] public float height;
    public bool enableOnAwake = true;
    public bool showOnlyOnProgress = false;

    [SerializeField] private GameObject m_background = null;
    [SerializeField] private GameObject m_foreground = null;

    //public bool CanReset { get; set; }

    public virtual bool IsSetPositionReqiured => true;
    //public virtual bool StartFull => false;

    public T ProgressBar
    {
        get => m_progressBar;
        protected set => m_progressBar = value;
    }

    [ShowOnly, SerializeField] private T m_progressBar;

    protected SpriteRenderer spriteRenderer;

    protected virtual void Awake()
    {
        ProgressBar = GetProgressBar();
        width = m_background.transform.localScale.x;
        height = m_background.transform.localScale.y;
        //transform.localScale = new Vector3(width, height, 1f);

        spriteRenderer = m_foreground.GetComponent<SpriteRenderer>();

        //if (StartFull)
        //    ProgressBar.Value = ProgressBar.Maximum;

        //if (showOnlyOnProgress)
        //    m_spriteRenderer.enabled=(false);

        if (!enableOnAwake)
            //enabled = false;
            gameObject.SetActive(false);
    }

    protected virtual void LateUpdate()
    {
        if (ProgressBar != null)
        {
            SetForeground();
            SetColour();

            //if (showOnlyOnProgress)
            //    gameObject.SetActive(ProgressBar.Fraction != 1f);
        }
    }

    protected virtual void SetForeground()
    {
        //
        // Do I need to take 'progressBar.Minimum' into account?
        //
        if (IsSetPositionReqiured)
        {
            float fgWidth = width * ProgressBar.Fraction;
            m_foreground.transform.SetLocalX(m_background.transform.GetLocalX() - (width - fgWidth) / 2f);
        }

        //foreground.transform.localScale = foreground.transform.localScale.SetX(fgWidth);
        m_foreground.transform.localScale = m_foreground.transform.localScale.SetX(width * ProgressBar.Fraction);
    }

    protected virtual void SetColour()
    {
        if (spriteRenderer)
            spriteRenderer.color = CalcColour();
    }

    public void SetProgress(float max, float value)
    {
        ProgressBar.UpdateValues(max, value);
        gameObject.SetActive(true);
    }

    public void ResetProgress()
    {
        //if (CanReset)
        {
            ProgressBar.Value = 0f;
            gameObject.SetActive(false);
        }
    }

    protected abstract T GetProgressBar();
    public abstract Color CalcColour();
}