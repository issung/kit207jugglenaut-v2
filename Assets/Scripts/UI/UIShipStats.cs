﻿using System;

public struct UIShipStats
{
    public string Name { get; }
    public string HullStrength { get; }
    public string Speed { get; }
    public string WeaponDamage { get; }
    public string WeaponFireRate { get; }

    public UIShipStats(string name, string hullStrength, string speed, string weaponDamage, string weaponFireRate) : this()
    {
        Name = name;
        HullStrength = hullStrength ?? throw new ArgumentNullException(nameof(hullStrength));
        Speed = speed ?? throw new ArgumentNullException(nameof(speed));
        WeaponDamage = weaponDamage ?? throw new ArgumentNullException(nameof(weaponDamage));
        WeaponFireRate = weaponFireRate ?? throw new ArgumentNullException(nameof(weaponFireRate));
    }
}