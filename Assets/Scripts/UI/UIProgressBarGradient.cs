﻿using UnityEngine;

public abstract class UIProgressBarGradient<T> : UIProgressBar<T> where T : ProgressBar
{
    public Color StartColour;
    public Color EndColour;

    //protected override T GetProgressBar() => GetComponent<T>();

    public override Color CalcColour() => Color.Lerp(StartColour, EndColour, 1f - ProgressBar.Fraction);
}