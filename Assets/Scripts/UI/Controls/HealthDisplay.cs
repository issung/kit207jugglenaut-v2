﻿public class HealthDisplay : UIProgressBarGradient<Health>
{
    protected override Health GetProgressBar() => GetComponentInParent<Health>();
}