﻿using UnityEngine;
using UnityEngine.UI;

public abstract class ButtonHandler : MonoBehaviour
{
    protected Button m_button;

    protected abstract UIButtonSoundType PressSoundType { get; }

    private void Awake()
    {
        m_button = GetComponent<Button>();

        m_button.onClick.AddListener(() =>
        {
            AudioManager.Instance.PlayUIButtonClip(PressSoundType);
            OnClick();
        });
    }

    private void OnDestroy() => m_button.onClick.RemoveListener(OnClick);

    public abstract void OnClick();
}