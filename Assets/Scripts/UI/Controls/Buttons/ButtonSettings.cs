﻿using UnityEngine.SceneManagement;

public class ButtonSettings : ButtonHandler
{
    protected override UIButtonSoundType PressSoundType => UIButtonSoundType.Confirm;

    public override void OnClick() => SceneManager.LoadScene("Settings");
}