﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonMainMenu : ButtonHandler
{
    protected override UIButtonSoundType PressSoundType => UIButtonSoundType.Confirm;
    
    public override void OnClick()
    {
        IngameMusic.Instance.StopMusic();
        SceneManager.LoadScene("MainMenu");
    }
}