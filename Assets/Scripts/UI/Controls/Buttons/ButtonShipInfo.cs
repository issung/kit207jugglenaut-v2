﻿using UnityEngine.SceneManagement;

public class ButtonShipInfo : ButtonHandler
{
    protected override UIButtonSoundType PressSoundType => UIButtonSoundType.Confirm;

    public override void OnClick() => SceneManager.LoadScene("ShipInfo");
}