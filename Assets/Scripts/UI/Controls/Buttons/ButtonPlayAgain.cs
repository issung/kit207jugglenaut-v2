﻿using UnityEngine.SceneManagement;

public class ButtonPlayAgain : ButtonHandler
{
    protected override UIButtonSoundType PressSoundType => UIButtonSoundType.Confirm;

    public override void OnClick() => SceneManager.LoadScene("GameScene");
}