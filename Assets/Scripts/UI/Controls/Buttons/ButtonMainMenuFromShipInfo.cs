﻿using UnityEngine.SceneManagement;

public class ButtonMainMenuFromShipInfo : ButtonHandler
{
    protected override UIButtonSoundType PressSoundType => UIButtonSoundType.BackOrCancel;

    public override void OnClick() => SceneManager.LoadScene("MainMenu");
}