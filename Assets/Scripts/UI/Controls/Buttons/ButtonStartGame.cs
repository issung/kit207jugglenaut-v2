﻿using UnityEngine.SceneManagement;

public class ButtonStartGame : ButtonHandler
{
    protected override UIButtonSoundType PressSoundType => UIButtonSoundType.Confirm;

    public override void OnClick()
    {
        MainMenuMusic.Instance.Stop();
        SceneManager.LoadScene("GameScene");
    }
}