﻿using UnityEngine;

public class ButtonCloseGame : ButtonHandler
{
    protected override UIButtonSoundType PressSoundType => UIButtonSoundType.BackOrCancel;

    public override void OnClick() => Application.Quit();
}