﻿public class ShieldDisplay : UIProgressBarGradient<Shield>
{
    protected override Shield GetProgressBar() => GetComponentInParent<Shield>();
}