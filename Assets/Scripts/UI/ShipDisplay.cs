﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShipDisplay : MonoBehaviour
{
    #pragma warning disable CS0649

    [SerializeField] private Transform m_shipTransform;

    [Header("Models")]
    [SerializeField] private GameObject m_enemyPassiveDisplay;
    [SerializeField] private GameObject m_enemyGenericDisplay;
    [SerializeField] private GameObject m_enemyFastDisplay;
    [SerializeField] private GameObject m_enemyDodgerDisplay;
    [SerializeField] private GameObject m_enemyToughDisplay;

    [ShowOnly, SerializeField] private int m_currentShipIndex = 0;
    [ShowOnly, SerializeField] private int m_maxShips = -1;

    [SerializeField] private EnemyShipMaterial m_enemyShipDisplay;

    [Header("User Interface Controls")]
    [SerializeField] private Button m_buttonPrevious;
    [SerializeField] private Button m_buttonNext;

    [SerializeField] private TextMeshProUGUI m_textShipName;
    [SerializeField] private TextMeshProUGUI m_textShipHullStrength;
    [SerializeField] private TextMeshProUGUI m_textShipSpeed;
    [SerializeField] private TextMeshProUGUI m_textShipWeaponDamage;
    [SerializeField] private TextMeshProUGUI m_textShipWeaponFireRate;

    #pragma warning restore CS0649

    //private GameObject[] m_ships;
    private UIShipStats[] m_shipStats;

    private void Awake()
    {
        /*m_ships = new GameObject[]
        {
            Instantiate(m_enemyPassiveDisplay, m_shipTransform.position, m_shipTransform.rotation),
            Instantiate(m_enemyGenericDisplay, m_shipTransform.position, m_shipTransform.rotation),
            Instantiate(m_enemyFastDisplay, m_shipTransform.position, m_shipTransform.rotation),
            Instantiate(m_enemyDodgerDisplay, m_shipTransform.position, m_shipTransform.rotation),
            Instantiate(m_enemyToughDisplay, m_shipTransform.position, m_shipTransform.rotation)
        };*/

        m_shipStats = new UIShipStats[]
        {
            // Passive
            new UIShipStats("Passive", "Normal", "Normal", "None", "None"),

            // Generic
            new UIShipStats("Generic", "Normal", "Normal", "Normal", "Normal"),

            // Fast
            new UIShipStats("Fast", "Medium", "Fast", "Small", "Fast"),

            // Dodger
            new UIShipStats("Dodger", "Medium", "Slow", "Medium", "Normal"),

            // Tough
            new UIShipStats("Tough", "High", "Slow", "High", "Slow"),
        };

        //for (int i = 0; i < m_ships.Length; i++)
        //    m_ships[i].transform.localScale = m_shipTransform.localScale;

        m_maxShips = Enum.GetValues(typeof(EnemyType)).Length;

        m_currentShipIndex = 0;
        SetActiveShip(m_currentShipIndex);

        m_buttonPrevious.onClick.AddListener(OnButtonPreviousClick);
        m_buttonNext.onClick.AddListener(OnButtonNextClick);
    }

    private void OnDestroy()
    {
        m_buttonPrevious.onClick.RemoveListener(OnButtonPreviousClick);
        m_buttonNext.onClick.RemoveListener(OnButtonNextClick);
    }

    private void OnButtonPreviousClick()
    {
        if (--m_currentShipIndex < 0)
            m_currentShipIndex = m_maxShips - 1;

        AudioManager.Instance.PlayUIButtonClip(UIButtonSoundType.Confirm);
        SetActiveShip(m_currentShipIndex);
    }

    private void OnButtonNextClick()
    {
        if (++m_currentShipIndex > m_maxShips - 1)
            m_currentShipIndex = 0;

        AudioManager.Instance.PlayUIButtonClip(UIButtonSoundType.Confirm);
        SetActiveShip(m_currentShipIndex);
    }

    private void SetActiveShip(int activeShipIndex)
    {
        //for (int i = 0; i < m_ships.Length; i++)
        //    m_ships[i].SetActive(i == activeShipIndex);

        //m_ships[activeShipIndex].GetComponent<EnemyShipDisplay>().ResetTransform();

        m_enemyShipDisplay.EnemyType = (EnemyType)m_currentShipIndex;
        m_enemyShipDisplay.ReloadShipMaterials();

        m_textShipName.text = $"Ship Name:\n{m_shipStats[activeShipIndex].Name}";
        m_textShipHullStrength.text = $"Hull Strength:\n{m_shipStats[activeShipIndex].HullStrength}";
        m_textShipSpeed.text = $"Speed:\n{m_shipStats[activeShipIndex].Speed}";
        m_textShipWeaponDamage.text = $"Weapon Damage:\n{m_shipStats[activeShipIndex].WeaponDamage}";
        m_textShipWeaponFireRate.text = $"Weapon Fire Rate:\n{m_shipStats[activeShipIndex].WeaponFireRate}";
    }
}