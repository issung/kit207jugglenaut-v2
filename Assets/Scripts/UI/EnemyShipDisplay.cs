﻿using UnityEngine;

public class EnemyShipDisplay : MonoBehaviour
{
    public Vector3 RotationSpeed;

    [SerializeField] private float m_moveHeight = 1f;
    [SerializeField] private float m_speed = 3f;
    [SerializeField] private float m_yPositionStart;

    private float m_yPositionReset;
    private float m_yRotation, m_yPosition;
    private float m_randomTimeOffset;

    private float m_lastDisableTime;
    private float m_disableDuration;

    private void Awake()
    {
        m_yPositionReset = gameObject.transform.GetY();
        m_yPositionStart = gameObject.transform.GetY() + m_moveHeight / 2f;
        m_randomTimeOffset = Random.Range(0f, 2f);
    }

    private void FixedUpdate()
    {
        m_yPosition = m_yPositionStart + (Mathf.Sin((Time.time + m_randomTimeOffset - m_disableDuration) * m_speed) * m_moveHeight / 2f);
        transform.SetY(m_yPosition);

        transform.Rotate(
            RotationSpeed.x * Time.deltaTime,
            RotationSpeed.y * Time.deltaTime,
            RotationSpeed.z * Time.deltaTime, Space.Self);
    }

    private void OnDisable()
    {
        m_lastDisableTime = Time.time;
    }

    private void OnEnable()
    {
        transform.SetY(m_yPositionReset);
        transform.rotation = Quaternion.identity;
        m_disableDuration = Time.time - m_lastDisableTime;
    }
}