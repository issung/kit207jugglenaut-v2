﻿using UnityEngine;

public class ShipDisplayMenu : MonoBehaviour
{
    public float yRotationSpeed;
    
    private void Update()
    {
        transform.Rotate(0f, yRotationSpeed * Time.deltaTime, 0f, Space.Self);
    }
}