﻿using UnityEngine;

public class ProgressBar : MonoBehaviour, IProgressBar
{
    //
    // I did not use the 'm_pPropertyName' convention with most of the property's variables for readability in the editor.
    //
    [ShowOnly, SerializeField] private float m_minimum;
    [/*ShowOnly,*/ SerializeField] private float m_maximum;
    [ShowOnly, SerializeField] private float m_value;
    [ShowOnly, SerializeField] private float m_fraction;

    public float Minimum { get => m_minimum; set => m_minimum = value; }
    public float Maximum { get => m_maximum; set => m_maximum = value; }

    public float Value
    {
        get => m_value;
        set
        {
            if (m_value != value)
            {
                m_value = Mathf.Clamp(value, Minimum, Maximum);
                OnValueChanged();
            }
        }
    }

    public virtual ProgressBarStartingValue StartValue { get; }

    //public event EventHandler ValueChanged;

    protected virtual void Awake()
    {
        Value = StartValue == ProgressBarStartingValue.Minimum ? 0 : Maximum;
    }

    public float Fraction
    {
        get
        {
            m_fraction = Value / Maximum;

            if (float.IsInfinity(m_fraction) ||
                float.IsNaN(m_fraction))
            {
                Debug.LogWarning($"{nameof(ProgressBar)}.{nameof(Fraction)}: Almost returned {m_fraction}. Value = {Value}, Maximum = {Maximum}");
                //Debug.LogWarning($"{nameof(ProgressBar)}.{nameof(Fraction)}: Almost returned infinite or NaN");
                return 1f;
            }

            return Mathf.Clamp01(Value / Maximum);//(Value - Minimum) / (Maximum - Minimum);
        }
    }

    public void UpdateValues(/*float min, */float max, float value)
    {
        //Minimum = min;
        Maximum = max;
        Value = value;
    }

    protected virtual void OnValueChanged() { }
    //protected void OnValueChanged() => ValueChanged?.Invoke(this, EventArgs.Empty);
}