﻿using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
#pragma warning disable IDE0044, CS0649

    [SerializeField] private bool m_center;
    [SerializeField] private float time;
    private float width;

#pragma warning restore IDE0044, CS0649

    void Awake()
    {
        width = transform.localScale.x;

        if (m_center)
        {
            LeanTween.value(gameObject, (float x) =>
            {
                transform.position = new Vector3(x, transform.position.y, transform.position.z);
            }, transform.position.x, -width, time).setOnComplete(() => { MoveFromStartToEnd(); });
        }
        else
            MoveFromStartToEnd();
    }

    private void MoveFromStartToEnd()
    {
        float from = width - 2f; /* Subtracting 1 to get rid of the strange gap between the objects. */
        float to = -width;

        LeanTween.value(gameObject, (float x) =>
        {
            transform.position = new Vector3(x, transform.position.y, transform.position.z);
        }, from, to, time * 2).setOnComplete(() => { MoveFromStartToEnd(); });
    }
}