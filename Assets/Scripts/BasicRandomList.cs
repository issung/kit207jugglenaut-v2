﻿using System.Collections.Generic;
using UnityEngine;

public class BasicRandomList<T> : List<T>
{
    public T Random => this[UnityEngine.Random.Range(0, Count)];
}