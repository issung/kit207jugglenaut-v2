﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Controller : UnitySingleton<Controller>
{
    //public float GameBoundsXMin;
    //public float GameBoundsXMax;
    //public float GameBoundsYMin;
    //public float GameBoundsYMax;
    public Bounds GameBounds;
    //public float EnemyStopX;

    // Aiden was here

    #pragma warning disable IDE0044, CS0649

    [SerializeField] private bool m_showGameBounds;
    [SerializeField] private GameObject m_bulletPrefabDrag;
    
    #pragma warning restore IDE0044, CS0649

    private static GameObject m_bulletPrefab;
    //private static Queue<Bullet> bullets;
    private static List<Bullet> m_bulletPool;
    private static float m_dtf;
    
    public int fpsTarget = 60;
    public float timeScale = 1;

    private Vector3 m_gameBoundsCentre;
    private Vector3 m_gameBoundsSize;
    private Color m_gameBoundsColour;

    private void OnValidate() => UpdateGameBounds();

    public override void Awake()
    {
        base.Awake();

        //bullets = new Queue<Bullet>();
        m_bulletPool = new List<Bullet>();
        m_bulletPrefab = m_bulletPrefabDrag;
        UpdateGameBounds();
    }

    private void UpdateGameBounds()
    {
        //m_gameBoundsCentre = new Vector3(GameBoundsXMin+GameBoundsXMax, GameBoundsYMin+GameBoundsYMax, 0f);
        //m_gameBoundsSize = new Vector3(GameBoundsXMax-GameBoundsXMin, GameBoundsYMax-GameBoundsYMin, 0.0125f);
        m_gameBoundsColour = new Color(1f, 1f, 0f, 0.125f);
    }

    private void Update()
    {
        Application.targetFrameRate = fpsTarget;
        Time.timeScale = timeScale;

        m_dtf = 60f / (1.0f / Time.deltaTime);
    }

    public void StartGame()
    {
        HUD.Instance.StartGame();
        WaveSpawner.Instance.StartSpawning();
        PickupSpawner.Instance.StartSpawning();
    }

    public static void QueueBullet(Bullet bullet)
    {
        bullet.gameObject.SetActive(false);
        Instance.InvokeDelay(0.1f, () => { m_bulletPool.Add(bullet); });
    }

    public static float GetDtf()
    {
        return m_dtf;
    }

    /// <summary>
    /// Needs work, needs to dequeue the needed type of bullet, Queue might need to be turned into a list.
    /// </summary>
    public static Bullet GetBullet(Bullet.Type type)
    {
        List<Bullet> bulletsOfType = m_bulletPool.Where(t => t.type == type).ToList();

        if (bulletsOfType.Count < 1)
        {
            GameObject bullet = Instantiate(m_bulletPrefab);

            if (type == Bullet.Type.Direction)
                bullet.AddComponent(typeof(DirectionBullet));

            return bullet.GetComponent<Bullet>();
        }
        else
        {
            Bullet bullet = bulletsOfType[0];
            m_bulletPool.Remove(bullet);
            return bullet;
        }
    }

    public void ClampTransformToGameBounds(Transform transform, ClampBoundsFlags flags) => Utils.ClampTransformToBounds(transform, GameBounds, flags);

    public void ClampTransformToGameBounds(Transform transform) => ClampTransformToGameBounds(transform, ClampBoundsFlags.None);

    private void OnDrawGizmos()
    {
        if (m_showGameBounds)
        {
            Gizmos.color = m_gameBoundsColour;
            Gizmos.DrawCube(GameBounds.center, GameBounds.size);
            //Gizmos.DrawCube(m_gameBoundsCentre, m_gameBoundsSize);
        }
    }
}