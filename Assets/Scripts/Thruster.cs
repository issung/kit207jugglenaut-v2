﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class Thruster : MonoBehaviour
{
    private MeshFilter m_meshFilter;
    private Mesh m_mesh;
    private Vector3[] m_vertices;

    private WaitForSeconds m_updateInterval;
    private Coroutine m_updateCoroutine;

    private void Awake()
    {
        m_mesh = GetComponent<MeshFilter>().mesh;
        m_vertices = new Vector3[m_mesh.vertexCount];
        m_updateInterval = new WaitForSeconds(0.05f);
        m_updateCoroutine = StartCoroutine(UpdateCoroutine());
    }

    private void OnDestroy()
    {
        if (m_updateCoroutine != null)
            StopCoroutine(m_updateCoroutine);
    }

    private IEnumerator UpdateCoroutine()
    {
        while (true)
        {
            for (int i = 0; i < m_mesh.vertices.Length; i++)
            {
                m_vertices[i] = m_mesh.vertices[i];
                m_vertices[i].y = Mathf.Clamp(m_vertices[i].y + Random.Range(-0.1f, 0.1f), -1, 0);
            }

            m_mesh.SetVertices(m_vertices.ToList());

            yield return m_updateInterval;
        }
    }
}
