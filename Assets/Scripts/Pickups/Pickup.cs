﻿using UnityEngine;

public abstract class Pickup : MonoBehaviour
{
    protected abstract AudioClip PickupSound { get; }

    public float UpDownHeight = 1f;
    public float UpDownSpeed = 2f;
    public float RotateSpeed = 1f;
    public float MoveSpeed = 0.005f;

    private Vector3 m_direction = Vector3.left;
    private float m_yRotation, m_yPosition;

    [SerializeField, ShowOnly] private float m_yPositionStart;
    //[SerializeField, ShowOnly] private float m_yRotationStart;

    private Player m_player;
    private float m_randomTimeOffset;

    private Vector3 m_growTo;

    protected virtual void Start()
    {
        m_yPositionStart = transform.GetY() + UpDownHeight / 2f;
        //m_yRotationStart = transform.rotation.eulerAngles.y;
        m_randomTimeOffset = Random.Range(0f, 2f);

        m_growTo = transform.localScale;
        transform.localScale = Vector3.zero;
        LeanTween.value(gameObject, (Vector3 v3) => { transform.localScale = v3; }, Vector3.zero, m_growTo, 2f);
    }

    protected virtual void Update()
    {
        m_yPosition = m_yPositionStart + (Mathf.Sin((Time.time + m_randomTimeOffset) * UpDownSpeed) * UpDownHeight) / 2f;
        transform.SetY(m_yPosition);

        m_yRotation += RotateSpeed * Controller.GetDtf();
        transform.SetYRotation(/*m_yRotationStart +*/ m_yRotation);

        transform.position += m_direction * MoveSpeed * Controller.GetDtf();

        if (transform.GetX() < Controller.Instance.GameBounds.min.x)
        {
            PickupSpawner.Instance.currentPickups--;
            Destroy(gameObject);
        }
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        m_player = other.gameObject.GetComponent<Player>();

        if (other.gameObject.tag == "Player" && m_player && !m_player.IsDead && OnPickup(m_player))
        {
            PickupSpawner.Instance.currentPickups--;

            if (PickupSound)
                AudioManager.Instance.player.PlayAndWait(PickupSound);

            Destroy(gameObject);
        }
    }

    protected abstract bool OnPickup(Player player);
}