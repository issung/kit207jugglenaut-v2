﻿using UnityEngine;

public class PickupX2 : Pickup
{
    protected override AudioClip PickupSound => AudioManager.Instance.pickupX2;

    public float effectDuration;
    public float timeBetweenShots;

    protected override bool OnPickup(Player player)
    {
        if (player.AutoShoot && !player.AutoShoot.IsFireRateModified)
        {
            PopupTextController.Popup("Weapon Powerup!", PopupText.TextColor.Green, transform.position);
            player.AutoShoot.StartCoroutine(player.AutoShoot.ModifyShootRate(effectDuration, timeBetweenShots));
            return true;
        }

        return false;
    }
}