﻿using UnityEngine;

public class PickupHealthPack : Pickup
{
    protected override AudioClip PickupSound => AudioManager.Instance.pickupHealth;

    public int Health = 10;

    protected override bool OnPickup(Player player)
    {
        if (player.CanPickupHealthPack)
        {
            player.AddHealth(Health);
            PopupTextController.Popup($"+{Health} HP", PopupText.TextColor.Green, transform.position);
            return true;
        }

        return false;
    }
}