﻿using UnityEngine;

public class PickupShield : Pickup
{
    protected override AudioClip PickupSound => AudioManager.Instance.pickupShield;

    protected override bool OnPickup(Player player)
    {
        if (player.CanPickupShield)
        {
            player.ActivateShields();
            PopupTextController.Popup("Shield Powerup!", PopupText.TextColor.Green, transform.position);
            return true;
        }

        return false;
    }
}