﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : UnitySingleton<WaveSpawner>
{
    public override bool Persistent => false;

    public WaveInfo[] Waves { get; private set; }
    
    #pragma warning disable IDE0044, CS0649

    [SerializeField] private bool m_debug;

    public Bounds SpawnBounds;
    public float WaveStartDelay = 1f;
    public float SpawnDelayMin = 1f;
    public float SpawnDelayMax = 3f;
    public int currentWave = 0;
    public int currentEnemyIndex = 0;

    /// <summary>
    /// "currentWave" but not capped out and keeps incrementing for the player to see.
    /// </summary>
    public int visualCurrentWave = 0;

    [ShowOnly] public int enemiesSpawned;
    [ShowOnly] public int enemiesLeft;

    [ShowOnly] public List<GameObject> Spawned;
    [ShowOnly, SerializeField] private bool m_waveStarted;

    [ShowOnly, SerializeField] private float m_lastSpawnTime;

    [ShowOnly, SerializeField] private float m_nextSpawn;
    [ShowOnly, SerializeField] private bool m_lastWave;
    //[ShowOnly, SerializeField] private bool m_allWavesCompleted;

    #pragma warning restore IDE0044, CS0649

    public int EnemiesInWave { get; private set; }

    public bool WaveStarted => m_waveStarted;

    private bool m_gameEndShown;
    private Coroutine m_updateWaveCoroutine;
    private Coroutine m_killAllOnScreenCoroutine;

    public override void Awake()
    {
        base.Awake();

        Spawned = new List<GameObject>(20);

        #region Test Logic

        /*List<GameObject> waveEnemies = new List<GameObject>();

        EnemySpawnInfo info = new EnemySpawnInfo(3, EnemyType.Generic);

        for (int i = 0; i < Waves.Length; i++)
        {
            for (int j = 0; j < Waves[i].EnemySpawns.Length; j++)
            {
                waveEnemies.Add(PrefabLinks.Instance.GetEnemy(info.Type));
            }
        }*/

        #endregion

        m_waveStarted = false;

        Waves = new WaveInfo[]
        {
            new WaveInfo(
                2f, "Wave 1",
                new EnemySpawnInfo(5, EnemyType.Passive)
            ),

            new WaveInfo(
                2f, "Wave 2",
                new EnemySpawnInfo(1, EnemyType.Passive),
                new EnemySpawnInfo(8, EnemyType.Generic)
            ),

            new WaveInfo(
                2f, "Wave 3",
                new EnemySpawnInfo(10, EnemyType.Generic),
                new EnemySpawnInfo(4, EnemyType.Fast),
                new EnemySpawnInfo(5, EnemyType.Generic)
            ),

            new WaveInfo(
                2f, "Wave 4",
                new EnemySpawnInfo(4, EnemyType.Generic),
                new EnemySpawnInfo(8, EnemyType.Fast),
                new EnemySpawnInfo(3, EnemyType.Generic),
                new EnemySpawnInfo(1, EnemyType.Tough)
            ),

            new WaveInfo(
                2f, "Wave 5",
                new EnemySpawnInfo(12, EnemyType.Fast),
                new EnemySpawnInfo(8, EnemyType.Generic),
                new EnemySpawnInfo(4, EnemyType.Tough),
                new EnemySpawnInfo(5, EnemyType.Dodger),
                new EnemySpawnInfo(8, EnemyType.Fast)
            ),

            new WaveInfo(
                2f, "Wave 6",
                new EnemySpawnInfo(20, EnemyType.Fast),
                new EnemySpawnInfo(4, EnemyType.Generic),
                new EnemySpawnInfo(14, EnemyType.Tough),
                new EnemySpawnInfo(8, EnemyType.Dodger),
                new EnemySpawnInfo(6, EnemyType.Fast),
                new EnemySpawnInfo(6, EnemyType.Tough)
            ),

            // Wave #6 (INSANE)
            new WaveInfo(
                3f,
                "Wave VCW",
                new EnemySpawnInfo(16, EnemyType.Tough),
                new EnemySpawnInfo(10, EnemyType.Dodger),
                new EnemySpawnInfo(12, EnemyType.Fast),
                new EnemySpawnInfo(4, EnemyType.Dodger),
                new EnemySpawnInfo(6, EnemyType.Tough),
                new EnemySpawnInfo(8, EnemyType.Fast),
                new EnemySpawnInfo(24, EnemyType.Tough)
            )
        };
    }

    public void StartSpawning() => m_updateWaveCoroutine = StartCoroutine(UpdateWaveCoroutine());

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            if (m_killAllOnScreenCoroutine != null)
                StopCoroutine(m_killAllOnScreenCoroutine);

            m_killAllOnScreenCoroutine = StartCoroutine(KillAllOnScreen());
        }
    }

    private void OnDestroy()
    {
        if (m_killAllOnScreenCoroutine != null)
            StopCoroutine(m_killAllOnScreenCoroutine);
    }

    private IEnumerator UpdateWaveCoroutine()
    {
        while (true)
        {
            if (!m_waveStarted && enemiesSpawned <= 0 && !m_lastWave)
            {
                EnemiesInWave = 0;

                for (int i = 0; i < Waves[currentWave].EnemySpawns.Length; i++)
                    EnemiesInWave += Waves[currentWave].EnemySpawns[i].SpawnAmount;

                enemiesLeft = EnemiesInWave;

                if (Waves[currentWave].HasWaveIntro)
                {
                    string waveIntroText = Waves[currentWave].WaveIntroText.Replace("VCW", (visualCurrentWave + 1).ToString());
                    HUD.Instance.UpdateWaveIntroText(waveIntroText);
                    yield return new WaitForSeconds(Waves[currentWave].WaveStartDelay);
                    HUD.Instance.UpdateWaveIntroText("");
                }
                //else
                //    yield return new WaitForSeconds(Waves[currentWave].WaveStartDelay);

                currentEnemyIndex = 0;
                m_waveStarted = true;
            }

            if (m_waveStarted)
            {
                m_nextSpawn = Random.Range(SpawnDelayMin, SpawnDelayMax);

                Spawned.Add(Instantiate(
                    PrefabLinks.Instance.GetEnemy(Waves[currentWave].EnemySpawns[currentEnemyIndex].Type),
                    new Vector3(
                        SpawnBounds.min.x,
                        Random.Range(SpawnBounds.min.y, SpawnBounds.max.y)),
                    Quaternion.identity));
                HUD.Instance.UpdateWaveBar();

                m_lastSpawnTime = Time.time;

                // This current implementation only spawns all of each enemy.
                if (--Waves[currentWave].EnemySpawns[currentEnemyIndex].SpawnAmount <= 0)
                    if (++currentEnemyIndex >= Waves[currentWave].EnemySpawns.Length)
                        m_waveStarted = false;

                yield return new WaitForSeconds(m_nextSpawn);
            }
            else
                yield return null;
        }
    }
    
    private IEnumerator KillAllOnScreen()
    {
        int index = 0;

        while (/*enemiesLeft > 0 &&*/ index < Spawned.Count)
        {
            Spawned[index].GetComponent<Enemy>().Kill();
            //m_spawned.RemoveAt(index);
            index++;

            yield return new WaitForSeconds(Random.Range(0.1f, 0.2f));
        }

        Spawned.Clear();
    }

    public void UpdateSpawner()
    {
        HUD.Instance.UpdateWaveBar();

        if (enemiesLeft <= 0)
        {
            visualCurrentWave++;
            currentWave++;

            // Clamp currentwave to be stuck within the limit (last wave infinite).
            if (currentWave > Waves.Length - 1)
            {
                currentWave = Waves.Length - 1;
            }

            HUD.Instance.UpdateWaveText(visualCurrentWave);

            /*if (!m_gameEndShown && m_lastWave)
            {
                //m_allWavesCompleted = true;
                HUD.Instance.SetGameEnded(true);
                m_gameEndShown = true;
            }*/
        }
    }

    private void OnDrawGizmos()
    {
        if (m_debug)
        {
            Gizmos.color = new Color(0f, 1f, 1f, 0.4f);
            Gizmos.DrawCube(SpawnBounds.center, SpawnBounds.size);
        }
    }

    private void OnGUI()
    {
        if (m_debug)
        {
            string text =
                $"Current Enemy Index: {currentEnemyIndex}\n" +
                $"Next enemy in: {m_nextSpawn - (Time.time - m_lastSpawnTime):N1}s\n" +
                $"Enemies Spawned: {enemiesSpawned}";

            var textSize = GUI.skin.box.CalcSize(new GUIContent(text));

            MyDebug.GuiAutoBox(
                Screen.width - textSize.x - 16f,
                Screen.height - textSize.y - 16f, text);
        }
    }
}