﻿public struct EnemySpawnInfo
{
    public int SpawnAmount { get; set; }
    public EnemyType Type { get; set; }

    public EnemySpawnInfo(int spawnAmount, EnemyType type) : this()
    {
        SpawnAmount = spawnAmount;
        Type = type;
    }
}