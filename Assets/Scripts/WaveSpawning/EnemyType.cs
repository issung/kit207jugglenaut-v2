﻿public enum EnemyType
{
    Passive,
    Generic,
    Fast,
    Dodger,
    Tough
}