﻿using System;

/// <summary>
/// This class is work in progress and is likely to change in the future.
/// </summary>
public struct WaveInfo
{
    //public float StartDeley { get; set; }
    //public int Round { get; set; }
    public EnemySpawnInfo[] EnemySpawns { get; set; }

    public float WaveStartDelay { get; set; }
    public string WaveIntroText { get; set; }

    public bool HasWaveIntro => !string.IsNullOrWhiteSpace(WaveIntroText) && WaveStartDelay > 0f;

    public WaveInfo(float waveStartDelay, string waveIntroText, params EnemySpawnInfo[] enemySpawns) : this()
    {
        EnemySpawns = enemySpawns ?? throw new ArgumentNullException(nameof(enemySpawns));
        WaveStartDelay = waveStartDelay;
        WaveIntroText = waveIntroText ?? throw new ArgumentNullException(nameof(waveIntroText));
    }
    
    public WaveInfo(float waveStartDelay, params EnemySpawnInfo[] enemySpawns) : this(waveStartDelay, "", enemySpawns) { }
}